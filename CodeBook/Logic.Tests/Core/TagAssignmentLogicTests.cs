﻿using System;
using Api.Dto;
using Api.Interfaces.Repository;
using log4net;
using Logic.Core;
using Moq;
using NUnit.Framework;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class TagAssignmentLogicTests
    {
        private Mock<ILog> loggerMock_;
        private Mock<ISnippetRepository> snippetRepositoryMock_;
        private TagAssignmentLogic tagAssignmentLogic_;
        private Mock<ITagAssignmentRepository> tagAssignmentRepositoryMock_;
        private Mock<ITagRepository> tagRepositoryMock_;

        [Test, Category("Unit")]
        public void AssignTagToSnippet_CorrectArguments()
        {
            var snippetId = Guid.NewGuid();
            var tagName = "Tag";
            var tag = new Tag(tagName);
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippetId)).Returns(true);
            tagRepositoryMock_.Setup(_x => _x.TagExists(tagName)).Returns(true);
            tagRepositoryMock_.Setup(_x => _x.GetTag(tagName)).Returns(tag);
            tagAssignmentRepositoryMock_.Setup(_x => _x.AssignTagToSnippet(snippetId, tag.TagId)).Returns(true);
            Assert.AreEqual(true, tagAssignmentLogic_.AssignTagToSnippet(snippetId, tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            tagAssignmentRepositoryMock_.Verify(_x => _x.AssignTagToSnippet(snippetId, tag.TagId), Times.Once);
        }

        [Test, Category("Unit")]
        public void AssignTagToSnippet_SnippetDoesntExist()
        {
            var snippetId = Guid.NewGuid();
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippetId)).Returns(false);
            Assert.AreEqual(false, tagAssignmentLogic_.AssignTagToSnippet(snippetId, "Tag"));
            loggerMock_.Verify(_x => _x.Error(It.IsAny<string>()), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.AssignTagToSnippet(snippetId, It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void AssignTagToSnippet_TagDoesntExist()
        {
            var tagName = "Tag";
            var tag = new Tag(tagName);
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(It.IsAny<Guid>())).Returns(true);
            tagRepositoryMock_.Setup(_x => _x.TagExists(tagName)).Returns(false);
            tagRepositoryMock_.Setup(_x => _x.GetTag(tagName)).Returns(tag);
            tagRepositoryMock_.Setup(_x => _x.AddTag(tagName)).Returns(true);
            tagAssignmentRepositoryMock_.Setup(_x => _x.AssignTagToSnippet(It.IsAny<Guid>(), tag.TagId)).Returns(true);
            Assert.AreEqual(true, tagAssignmentLogic_.AssignTagToSnippet(Guid.NewGuid(), tagName));
            tagRepositoryMock_.Verify(_x => _x.AddTag(tagName), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.AssignTagToSnippet(It.IsAny<Guid>(), tag.TagId), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void AssignTagToSnippet_TagNameIsNullOrEmpty(string _tagName)
        {
            Assert.AreEqual(false, tagAssignmentLogic_.AssignTagToSnippet(Guid.NewGuid(), _tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.AssignTagToSnippet(It.IsAny<Guid>(), It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteAllLanguages()
        {
            tagAssignmentRepositoryMock_.Setup(_x => _x.DeleteAllAssignments()).Returns(true);
            Assert.AreEqual(true, tagAssignmentLogic_.DeleteAllAssignments());
            tagAssignmentRepositoryMock_.Verify(_x => _x.DeleteAllAssignments(), Times.Once);
        }

        [SetUp]
        public void SetUp()
        {
            loggerMock_ = new Mock<ILog>();
            loggerMock_.Setup(_x => _x.Warn(It.IsAny<string>()));
            loggerMock_.Setup(_x => _x.Error(It.IsAny<string>()));

            snippetRepositoryMock_ = new Mock<ISnippetRepository>();
            tagAssignmentRepositoryMock_ = new Mock<ITagAssignmentRepository>();
            tagRepositoryMock_ = new Mock<ITagRepository>();

            tagAssignmentLogic_ = new TagAssignmentLogic(
                loggerMock_.Object,
                snippetRepositoryMock_.Object,
                tagRepositoryMock_.Object,
                tagAssignmentRepositoryMock_.Object);
        }

        [Test, Category("Unit")]
        [TestCase(true)]
        [TestCase(false)]
        public void TagAssignmentExists(bool _exists)
        {
            var tag = new Tag("Tag");
            tagRepositoryMock_.Setup(_x => _x.GetTag(tag.TagName)).Returns(tag);
            tagAssignmentRepositoryMock_.Setup(_x => _x.TagAssignmentExists(It.IsAny<Guid>(), tag.TagId))
                .Returns(_exists);
            Assert.AreEqual(_exists, tagAssignmentLogic_.TagAssignmentExists(Guid.NewGuid(), tag.TagName));
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void TagAssignmentExists_TagNameIsNullOrEmpty(string _tagName)
        {
            Assert.AreEqual(false, tagAssignmentLogic_.TagAssignmentExists(Guid.NewGuid(), _tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.TagAssignmentExists(It.IsAny<Guid>(), It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        [TestCase(true, false, false, false)]
        [TestCase(false, true, false, false)]
        [TestCase(false, false, true, false)]
        [TestCase(false, false, false, true)]
        public void TagAssignmentLogicConstructor_IncorrectArguments(
            bool _isLoggerNull,
            bool _isSnippetRepositoryNull,
            bool _isTagRepositoryNull,
            bool _isTagAssignmentRepositoryNull)
        {
            var logger = _isLoggerNull ? null : loggerMock_.Object;
            var snippetRepository = _isSnippetRepositoryNull ? null : snippetRepositoryMock_.Object;
            var tagRepository = _isTagRepositoryNull ? null : tagRepositoryMock_.Object;
            var tagAssignmentRepository = _isTagAssignmentRepositoryNull ? null : tagAssignmentRepositoryMock_.Object;

            try
            {
                var tagAssignmentLogic = new TagAssignmentLogic(logger, snippetRepository, tagRepository, tagAssignmentRepository);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
        }

        [Test, Category("Unit")]
        public void UnassignTagFromSnippet_TagAssignmentDoesntExist()
        {
            var snippetId = Guid.NewGuid();
            var tag = new Tag("Tag");
            tagAssignmentRepositoryMock_.Setup(_x => _x.TagAssignmentExists(snippetId, tag.TagId)).Returns(false);
            Assert.AreEqual(false, tagAssignmentLogic_.UnassignTagFromSnippet(snippetId, tag.TagName));
            loggerMock_.Verify(_x => _x.Error(It.IsAny<string>()), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.UnassignTagFromSnippet(snippetId, tag.TagId), Times.Never);
        }

        [Test, Category("Unit")]
        public void UnassignTagFromSnippet_TagAssignmentExists()
        {
            var snippetId = Guid.NewGuid();
            var tagName = "Tag";
            var tag = new Tag(tagName);
            tagRepositoryMock_.Setup(_x => _x.GetTag(tagName)).Returns(tag);
            tagAssignmentRepositoryMock_.Setup(_x => _x.TagAssignmentExists(snippetId, tag.TagId)).Returns(true);
            tagAssignmentRepositoryMock_.Setup(_x => _x.UnassignTagFromSnippet(snippetId, tag.TagId)).Returns(true);
            Assert.AreEqual(true, tagAssignmentLogic_.UnassignTagFromSnippet(snippetId, tagName));
            tagAssignmentRepositoryMock_.Verify(_x => _x.UnassignTagFromSnippet(snippetId, tag.TagId), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void UnassignTagFromSnippet_TagNameIsNullOrEmpty(string _tagName)
        {
            Assert.AreEqual(false, tagAssignmentLogic_.UnassignTagFromSnippet(Guid.NewGuid(), _tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagAssignmentRepositoryMock_.Verify(_x => _x.UnassignTagFromSnippet(It.IsAny<Guid>(), It.IsAny<int>()), Times.Never);
        }
    }
}