﻿using System;

namespace Api.Dto
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()

    public class Flag : ICloneable
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        private string flagName_;

        internal Flag()
        {
        }

        public int FlagId { get; internal set; }

        public string FlagName
        {
            get => flagName_;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("FlagName cannot be null or empty!");
                flagName_ = value;
            }
        }

        public Flag(string _flagName)
        {
            FlagId = -1;
            FlagName = _flagName;
        }

        public object Clone()
        {
            return new Flag()
            {
                FlagId = FlagId,
                FlagName = FlagName
            };
        }

        public override bool Equals(object _object)
        {
            if (_object is Flag flag)
            {
                return FlagId.Equals(flag.FlagId)
                        && FlagName.Equals(flag.FlagName);
            }
            return false;
        }
    }
}