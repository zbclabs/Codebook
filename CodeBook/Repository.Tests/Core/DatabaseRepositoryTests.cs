﻿using System;
using System.IO;
using log4net;
using NUnit.Framework;
using Repository.Core;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class DatabaseRepositoryTests
    {
        private readonly string databaseFilePath_ = AppDomain.CurrentDomain.BaseDirectory + @"\CodeBook.db";
        private DatabaseRepository databaseRepository_;

        [Test, Category("Integration")]
        public void DatabaseRepositoryTest()
        {
            Assert.AreEqual(true, databaseRepository_.CreateDatabase());
            Assert.AreEqual(true, File.Exists(databaseFilePath_));
            Assert.AreEqual(true, databaseRepository_.DropDatabase());
            Assert.AreEqual(false, File.Exists(databaseFilePath_));
        }

        [SetUp]
        public void SetUp()
        {
            log4net.Config.BasicConfigurator.Configure(
                new log4net.Appender.ConsoleAppender
                {
                    Name = "ConsoleLogger",
                    Layout = new log4net.Layout.SimpleLayout()
                });
            var logger = LogManager.GetLogger("ConsoleLogger");
            if (File.Exists(databaseFilePath_)) File.Delete(databaseFilePath_);
            databaseRepository_ = new DatabaseRepository(databaseFilePath_, $"Data Source={databaseFilePath_}; Version=3", logger);
        }
    }
}