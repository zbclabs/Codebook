﻿using System;
using Api.Interfaces.Logic;

namespace Logic.Core
{
    public class ApplicationLogic : IApplicationLogic
    {
        public IDatabaseLogic DatabaseLogic { get; }

        public IFlagLogic FlagLogic { get; }

        public ILanguageLogic LanguageLogic { get; }

        public ISnippetLogic SnippetLogic { get; }

        public ITagAssignmentLogic TagAssignmentLogic { get; }

        public ITagLogic TagLogic { get; }

        public ApplicationLogic(

            IDatabaseLogic _databaseLogic,
            IFlagLogic _flagLogic,
            ILanguageLogic _languageLogic,
            ISnippetLogic _snippetLogic,
            ITagAssignmentLogic _tagAssignmentLogic,
            ITagLogic _tagLogic)
        {
            DatabaseLogic = _databaseLogic ?? throw new ArgumentException("DatabaseLogic cannot be null.");
            FlagLogic = _flagLogic ?? throw new ArgumentException("FlagLogic cannot be null.");
            LanguageLogic = _languageLogic ?? throw new ArgumentException("LanguageLogic cannot be null.");
            SnippetLogic = _snippetLogic ?? throw new ArgumentException("SnippetLogic cannot be null.");
            TagAssignmentLogic = _tagAssignmentLogic ?? throw new ArgumentException("TagAssignmentLogic cannot be null.");
            TagLogic = _tagLogic ?? throw new ArgumentException("TagLogic cannot be null.");
        }
    }
}