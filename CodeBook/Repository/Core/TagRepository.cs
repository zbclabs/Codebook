﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class TagRepository : BaseRepository, ITagRepository
    {
        public TagRepository(string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
        }

        public bool AddTag(string _tagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagName = _tagName
                    };
                    connection.Execute(SqlDictionary.AddTag, @params);
                    return true;
                }
            }, false);
        }

        public bool DeleteAllTags()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    connection.Execute(SqlDictionary.DeleteAllTags);
                    return true;
                }
            }, false);
        }

        public bool DeleteTag(string _tagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagName = _tagName
                    };
                    connection.Execute(SqlDictionary.DeleteTag, @params);
                    return true;
                }
            }, false);
        }

        public IEnumerable<string> GetAllTags()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    return connection.Query<string>(SqlDictionary.GetAllTags);
                }
            }, null);
        }

        public Tag GetTag(string _tagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagName = _tagName
                    };
                    return connection.Query<Tag>(SqlDictionary.GetTag, @params).SingleOrDefault();
                }
            }, null);
        }

        public bool ModifyTag(Tag _tag)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagId = _tag.TagId,
                        TagName = _tag.TagName
                    };
                    connection.Execute(SqlDictionary.ModifyTag, @params);
                    return true;
                }
            }, false);
        }

        public bool TagExists(string _tagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagName = _tagName
                    };
                    return connection.Query<bool>(SqlDictionary.TagExists, @params).Single();
                }
            }, false);
        }
    }
}