﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repository {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SqlDictionary {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SqlDictionary() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Repository.SqlDictionary", typeof(SqlDictionary).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Flags(FlagName) VALUES (@FlagName).
        /// </summary>
        internal static string AddFlag {
            get {
                return ResourceManager.GetString("AddFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Languages(LanguageName) VALUES (@LanguageName).
        /// </summary>
        internal static string AddLanguage {
            get {
                return ResourceManager.GetString("AddLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Snippets(
        ///	SnippetId,
        ///	LanguageId,
        ///	FlagId,
        ///	Title,
        ///	SnippetDescription,
        ///	Code,
        ///	CreationTime,
        ///	LastEditTime)
        ///VALUES (
        ///	@SnippetId,
        ///	@LanguageId,
        ///	@FlagId,
        ///	@Title,
        ///	@SnippetDescription,
        ///	@Code,
        ///	@CreationTime,
        ///	@LastEditTime).
        /// </summary>
        internal static string AddSnippet {
            get {
                return ResourceManager.GetString("AddSnippet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Tags(TagName) VALUES (@TagName).
        /// </summary>
        internal static string AddTag {
            get {
                return ResourceManager.GetString("AddTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TagAssignments(
        ///	TagAssignmentId,
        ///	SnippetId,
        ///	TagId)
        ///VALUES(
        ///	@TagAssignmentId,
        ///	@SnippetId,
        ///	@TagId).
        /// </summary>
        internal static string AssignTagToSnippet {
            get {
                return ResourceManager.GetString("AssignTagToSnippet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE Flags(
        ///	FlagId INTEGER PRIMARY KEY AUTOINCREMENT,
        ///	FlagName nvarchar(20) NOT NULL UNIQUE).
        /// </summary>
        internal static string CreateFlagsTable {
            get {
                return ResourceManager.GetString("CreateFlagsTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE Languages(
        ///	LanguageId INTEGER PRIMARY KEY AUTOINCREMENT,
        ///	LanguageName nvarchar(10) NOT NULL UNIQUE).
        /// </summary>
        internal static string CreateLanguagesTable {
            get {
                return ResourceManager.GetString("CreateLanguagesTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE Snippets(
        ///	SnippetId uniqueidentifier NOT NULL PRIMARY KEY,
        ///	LanguageId int NOT NULL,
        ///	FlagId int NOT NULL,
        ///	Title nvarchar(30) NOT NULL,
        ///	SnippetDescription nvarchar(100) NULL,
        ///	Code nvarchar(2147483647) NOT NULL,
        ///	CreationTime DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        ///	LastEditTime DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        ///	FOREIGN KEY (LanguageId) REFERENCES Languages(LanguageId),
        ///	FOREIGN KEY (FlagId) REFERENCES Flags(FlagId)).
        /// </summary>
        internal static string CreateSnippetsTable {
            get {
                return ResourceManager.GetString("CreateSnippetsTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE TagAssignments(
        ///	TagAssignmentId uniqueidentifier NOT NULL PRIMARY KEY,
        ///	SnippetId uniqueidentifier NOT NULL,
        ///	TagId int NOT NULL,
        ///	FOREIGN KEY (SnippetId) REFERENCES Snippets(SnippetId),
        ///	FOREIGN KEY (TagId) REFERENCES Tags(TagId),
        ///	CONSTRAINT SnippetTagUnique UNIQUE(SnippetId, TagId)).
        /// </summary>
        internal static string CreateTagAssignmentsTable {
            get {
                return ResourceManager.GetString("CreateTagAssignmentsTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE Tags(
        ///	TagId INTEGER PRIMARY KEY AUTOINCREMENT,
        ///	TagName nvarchar(10) NOT NULL UNIQUE).
        /// </summary>
        internal static string CreateTagsTable {
            get {
                return ResourceManager.GetString("CreateTagsTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM TagAssignments.
        /// </summary>
        internal static string DeleteAllAssignments {
            get {
                return ResourceManager.GetString("DeleteAllAssignments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Flags.
        /// </summary>
        internal static string DeleteAllFlags {
            get {
                return ResourceManager.GetString("DeleteAllFlags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Languages.
        /// </summary>
        internal static string DeleteAllLanguages {
            get {
                return ResourceManager.GetString("DeleteAllLanguages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Snippets.
        /// </summary>
        internal static string DeleteAllSnippets {
            get {
                return ResourceManager.GetString("DeleteAllSnippets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Tags.
        /// </summary>
        internal static string DeleteAllTags {
            get {
                return ResourceManager.GetString("DeleteAllTags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Flags WHERE FlagName = @FlagName.
        /// </summary>
        internal static string DeleteFlag {
            get {
                return ResourceManager.GetString("DeleteFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Languages WHERE LanguageName = @LanguageName.
        /// </summary>
        internal static string DeleteLanguage {
            get {
                return ResourceManager.GetString("DeleteLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Snippets WHERE SnippetId = @SnippetId.
        /// </summary>
        internal static string DeleteSnippet {
            get {
                return ResourceManager.GetString("DeleteSnippet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM Tags WHERE TagName = @TagName.
        /// </summary>
        internal static string DeleteTag {
            get {
                return ResourceManager.GetString("DeleteTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT EXISTS(SELECT 1 FROM Flags WHERE FlagName=@FlagName LIMIT 1).
        /// </summary>
        internal static string FlagExists {
            get {
                return ResourceManager.GetString("FlagExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT FlagName FROM Flags.
        /// </summary>
        internal static string GetAllFlags {
            get {
                return ResourceManager.GetString("GetAllFlags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT LanguageName FROM Languages.
        /// </summary>
        internal static string GetAllLanguages {
            get {
                return ResourceManager.GetString("GetAllLanguages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId.
        /// </summary>
        internal static string GetAllSnippets {
            get {
                return ResourceManager.GetString("GetAllSnippets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId
        ///WHERE F.FlagId = @FlagId.
        /// </summary>
        internal static string GetAllSnippetsWithFlag {
            get {
                return ResourceManager.GetString("GetAllSnippetsWithFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId
        ///WHERE L.LanguageId = @LanguageId.
        /// </summary>
        internal static string GetAllSnippetsWithLanguage {
            get {
                return ResourceManager.GetString("GetAllSnippetsWithLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT S.SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId JOIN TagAssignments AS A ON S.SnippetId = A.SnippetId
        ///WHERE A.TagId = @TagId.
        /// </summary>
        internal static string GetAllSnippetsWithTag {
            get {
                return ResourceManager.GetString("GetAllSnippetsWithTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId
        ///WHERE Title = @Title.
        /// </summary>
        internal static string GetAllSnippetsWithTitle {
            get {
                return ResourceManager.GetString("GetAllSnippetsWithTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT TagName FROM Tags.
        /// </summary>
        internal static string GetAllTags {
            get {
                return ResourceManager.GetString("GetAllTags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT * FROM Flags WHERE FlagName = @FlagName.
        /// </summary>
        internal static string GetFlag {
            get {
                return ResourceManager.GetString("GetFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT * FROM Languages WHERE LanguageName = @LanguageName.
        /// </summary>
        internal static string GetLanguage {
            get {
                return ResourceManager.GetString("GetLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT SnippetId, Title, SnippetDescription, Code, CreationTime, LastEditTime, L.LanguageId, L.LanguageName, F.FlagId, F.FlagName
        ///FROM Snippets AS S JOIN Languages AS L ON S.LanguageId = L.LanguageId JOIN Flags AS F ON S.FlagId = F.FlagId
        ///WHERE SnippetId = @SnippetId.
        /// </summary>
        internal static string GetSnippet {
            get {
                return ResourceManager.GetString("GetSnippet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT TagName FROM TagAssignments AS A JOIN Tags AS T ON A.TagId = T.TagId WHERE A.SnippetId = @SnippetId.
        /// </summary>
        internal static string GetSnippetTags {
            get {
                return ResourceManager.GetString("GetSnippetTags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT * FROM Tags WHERE TagName = @TagName.
        /// </summary>
        internal static string GetTag {
            get {
                return ResourceManager.GetString("GetTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT EXISTS(SELECT 1 FROM Languages WHERE LanguageName=@LanguageName LIMIT 1).
        /// </summary>
        internal static string LanguageExists {
            get {
                return ResourceManager.GetString("LanguageExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE Flags SET FlagName = @FlagName WHERE FlagId = @FlagId.
        /// </summary>
        internal static string ModifyFlag {
            get {
                return ResourceManager.GetString("ModifyFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE Languages SET LanguageName = @LanguageName WHERE LanguageId = @LanguageId.
        /// </summary>
        internal static string ModifyLanguage {
            get {
                return ResourceManager.GetString("ModifyLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE Snippets SET
        ///	LanguageId = @LanguageId,
        ///	FlagId = @FlagId,
        ///	Title = @Title,
        ///	SnippetDescription = @SnippetDescription,
        ///	Code = @Code,
        ///	CreationTime = @CreationTime,
        ///	LastEditTime = @LastEditTime WHERE SnippetId = @SnippetId.
        /// </summary>
        internal static string ModifySnippet {
            get {
                return ResourceManager.GetString("ModifySnippet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE Tags SET TagName = @TagName WHERE TagId = @TagId.
        /// </summary>
        internal static string ModifyTag {
            get {
                return ResourceManager.GetString("ModifyTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT EXISTS(SELECT 1 FROM Snippets WHERE SnippetId=@SnippetId LIMIT 1).
        /// </summary>
        internal static string SnippetExists {
            get {
                return ResourceManager.GetString("SnippetExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT EXISTS(SELECT 1 FROM TagAssignments WHERE SnippetId = @SnippetId AND TagId = @TagId).
        /// </summary>
        internal static string TagAssignmentExists {
            get {
                return ResourceManager.GetString("TagAssignmentExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT EXISTS(SELECT 1 FROM Tags WHERE TagName=@TagName LIMIT 1).
        /// </summary>
        internal static string TagExists {
            get {
                return ResourceManager.GetString("TagExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM TagAssignments WHERE SnippetId = @SnippetId AND TagId = @TagId.
        /// </summary>
        internal static string UnassignTagFromSnippet {
            get {
                return ResourceManager.GetString("UnassignTagFromSnippet", resourceCulture);
            }
        }
    }
}
