﻿using System;

namespace Api.Dto
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()

    public class Language : ICloneable
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        private string languageName_;

        internal Language()
        {
        }

        public int LanguageId { get; internal set; }

        public string LanguageName
        {
            get => languageName_;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("LanguageName cannot be null or empty!");
                languageName_ = value;
            }
        }

        public Language(string _languageName)
        {
            LanguageId = -1;
            LanguageName = _languageName;
        }

        public object Clone()
        {
            return new Language()
            {
                LanguageId = LanguageId,
                LanguageName = LanguageName
            };
        }

        public override bool Equals(object _object)
        {
            if (_object is Language language)
            {
                return LanguageId.Equals(language.LanguageId)
                        && LanguageName.Equals(language.LanguageName);
            }
            return false;
        }
    }
}