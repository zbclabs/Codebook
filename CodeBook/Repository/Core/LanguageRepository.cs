﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class LanguageRepository : BaseRepository, ILanguageRepository
    {
        public LanguageRepository(string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
        }

        public bool AddLanguage(string _languageName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageName = _languageName
                    };
                    connection.Execute(SqlDictionary.AddLanguage, @params);
                    return true;
                }
            }, false);
        }

        public bool DeleteAllLanguages()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    connection.Execute(SqlDictionary.DeleteAllLanguages);
                    return true;
                }
            }, false);
        }

        public bool DeleteLanguage(string _languageName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageName = _languageName
                    };
                    connection.Execute(SqlDictionary.DeleteLanguage, @params);
                    return true;
                }
            }, false);
        }

        public IEnumerable<string> GetAllLanguages()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    return connection.Query<string>(SqlDictionary.GetAllLanguages);
                }
            }, null);
        }

        public Language GetLanguage(string _languageName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageName = _languageName
                    };
                    return connection.Query<Language>(SqlDictionary.GetLanguage, @params).SingleOrDefault();
                }
            }, null);
        }

        public bool LanguageExists(string _languageName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageName = _languageName
                    };
                    return connection.Query<bool>(SqlDictionary.LanguageExists, @params).Single();
                }
            }, false);
        }

        public bool ModifyLanguage(Language _language)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageId = _language.LanguageId,
                        LanguageName = _language.LanguageName
                    };
                    connection.Execute(SqlDictionary.ModifyLanguage, @params);
                    return true;
                }
            }, false);
        }
    }
}