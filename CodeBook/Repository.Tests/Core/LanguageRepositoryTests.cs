﻿using System.Linq;
using NUnit.Framework;
using Repository.Core;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class LanguageRepositoryTests : RepositoryTestBase
    {
        private LanguageRepository languageRepository_;

        [Test, Category("Integration")]
        public void LanguageRepositoryTest()
        {
            // Języki jeszcze nie istnieją - te metody powinny zwrócić false lub null
            Assert.That(languageRepository_.LanguageExists("Java"), Is.EqualTo(false));
            Assert.That(languageRepository_.GetAllLanguages().ToList().Count, Is.EqualTo(0));
            Assert.That(languageRepository_.GetLanguage("Flag"), Is.EqualTo(null));

            // Utworzenie języków - te metody powinny przejść
            var languageName1 = "C#";
            var languageName2 = "VB.NET";
            Assert.That(languageRepository_.AddLanguage(languageName1), Is.EqualTo(true));
            Assert.That(languageRepository_.AddLanguage(languageName2), Is.EqualTo(true));

            // Utworzenie duplikatu - ta metoda powinna nawalić
            Assert.That(languageRepository_.AddLanguage(languageName1), Is.EqualTo(false));

            // Odczyt wszystkich języków
            var language1 = languageRepository_.GetLanguage(languageName1);
            Assert.AreNotEqual(null, language1);
            var language2 = languageRepository_.GetLanguage(languageName2);
            Assert.AreNotEqual(null, language2);
            var allFlags = languageRepository_.GetAllLanguages().ToList();
            Assert.That(allFlags.Contains(languageName1), Is.EqualTo(true));
            Assert.That(allFlags.Contains(languageName2), Is.EqualTo(true));

            // Sprawdzenie, czy języki istnieją
            Assert.That(languageRepository_.LanguageExists(languageName1), Is.EqualTo(true));
            Assert.That(languageRepository_.LanguageExists(languageName2), Is.EqualTo(true));
            Assert.That(languageRepository_.LanguageExists("Nope"), Is.EqualTo(false));

            // Modyfikacja języka
            language1.LanguageName = "Python";
            Assert.That(languageRepository_.ModifyLanguage(language1), Is.EqualTo(true));
            Assert.That(languageRepository_.GetLanguage(language1.LanguageName), Is.EqualTo(language1));

            // Usunięcie pojedynczego języka
            Assert.That(languageRepository_.DeleteLanguage(language1.LanguageName), Is.EqualTo(true));
            Assert.That(languageRepository_.LanguageExists(language1.LanguageName), Is.EqualTo(false));

            // Usunięcie wszystkich języków
            Assert.That(languageRepository_.DeleteAllLanguages(), Is.EqualTo(true));
            Assert.That(languageRepository_.GetAllLanguages().ToList().Count, Is.EqualTo(0));
        }

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            languageRepository_ = new LanguageRepository(ConnectionString, Logger);
        }

        [TearDown]
        public new void TearDown()
        {
            base.TearDown();
        }
    }
}