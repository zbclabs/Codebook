﻿using System;
using System.Collections.Generic;

namespace Api.Interfaces.Logic
{
    public interface ITagAssignmentLogic
    {
        bool AssignTagToSnippet(Guid _snippetId, string _tagName);

        bool DeleteAllAssignments();

        IEnumerable<string> GetSnippetTags(Guid _snippetId);

        bool TagAssignmentExists(Guid _snippetId, string _tagName);

        bool UnassignTagFromSnippet(Guid _snippetId, string _tagName);
    }
}