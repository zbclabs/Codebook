﻿using System;
using System.Collections.Generic;

namespace Api.Interfaces.Repository
{
    public interface ITagAssignmentRepository
    {
        bool AssignTagToSnippet(Guid _snippetId, int _tagId);

        bool DeleteAllAssignments();

        IEnumerable<string> GetSnippetTags(Guid _snippetId);

        bool TagAssignmentExists(Guid _snippetId, int _tagId);

        bool UnassignTagFromSnippet(Guid _snippetId, int _tagId);
    }
}