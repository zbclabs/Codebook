﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class FlagLogic : IFlagLogic
    {
        private readonly IFlagRepository flagRepository_;
        private readonly ILog logger_;

        public FlagLogic(ILog _logger, IFlagRepository _flagRepository)
        {
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
            flagRepository_ = _flagRepository ?? throw new ArgumentException("FlagRepository cannot be null!");
        }

        public Flag AddFlag(string _flagName)
        {
            if (string.IsNullOrEmpty(_flagName))
            {
                logger_.Warn("FlagName is null or empty, returning null...");
                return null;
            }
            flagRepository_.AddFlag(_flagName);
            return flagRepository_.GetFlag(_flagName);
        }

        public bool DeleteAllFlags()
        {
            return flagRepository_.DeleteAllFlags();
        }

        public bool DeleteFlag(string _flagName)
        {
            if (string.IsNullOrEmpty(_flagName))
            {
                logger_.Warn("FlagName is null or empty, returning false...");
                return false;
            }
            if (flagRepository_.FlagExists(_flagName))
                return flagRepository_.DeleteFlag(_flagName);
            logger_.Warn("Specified flag doesn't exist, no flag has been deleted.");
            return false;
        }

        public bool FlagExists(string _flagName)
        {
            if (!string.IsNullOrEmpty(_flagName)) return flagRepository_.FlagExists(_flagName);
            logger_.Warn("FlagName is null or empty, returning false...");
            return false;
        }

        public IEnumerable<string> GetAllFlags()
        {
            return flagRepository_.GetAllFlags();
        }

        public Flag GetFlag(string _flagName)
        {
            if (!string.IsNullOrEmpty(_flagName)) return flagRepository_.GetFlag(_flagName);
            logger_.Warn("FlagName is null or empty, returning null...");
            return null;
        }

        public bool ModifyFlag(Flag _flag)
        {
            if (_flag != null) return flagRepository_.ModifyFlag(_flag);
            logger_.Warn("Flag is null, returning false...");
            return false;
        }
    }
}