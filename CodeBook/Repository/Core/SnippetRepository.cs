﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class SnippetRepository : BaseRepository, ISnippetRepository
    {
        public SnippetRepository(string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
        }

        public bool AddSnippet(Snippet _snippet)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippet.SnippetId.ToString(),
                        LanguageId = _snippet.Language.LanguageId,
                        FlagId = _snippet.Flag.FlagId,
                        Title = _snippet.Title,
                        SnippetDescription = _snippet.SnippetDescription,
                        Code = _snippet.Code,
                        CreationTime = _snippet.CreationTime,
                        LastEditTime = _snippet.LastEditTime
                    };
                    connection.Execute(SqlDictionary.AddSnippet, @params);
                    return true;
                }
            }, false);
        }

        public bool DeleteAllSnippets()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    connection.Execute(SqlDictionary.DeleteAllSnippets);
                    return true;
                }
            }, false);
        }

        public bool DeleteSnippet(Guid _snippetId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString()
                    };
                    connection.Execute(SqlDictionary.DeleteSnippet, @params);
                    return true;
                }
            }, false);
        }

        public IEnumerable<Snippet> GetAllSnippets()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetAllSnippets,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, splitOn: "LanguageId, FlagId");
                }
            }, null);
        }

        public IEnumerable<Snippet> GetAllSnippetsWithFlag(int _flagId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagId = _flagId
                    };
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetAllSnippetsWithFlag,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, @params, splitOn: "LanguageId, FlagId");
                }
            }, null);
        }

        public IEnumerable<Snippet> GetAllSnippetsWithLanguage(int _languageId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        LanguageId = _languageId
                    };
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetAllSnippetsWithLanguage,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, @params, splitOn: "LanguageId, FlagId");
                }
            }, null);
        }

        public IEnumerable<Snippet> GetAllSnippetsWithTag(int _tagId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagId = _tagId
                    };
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetAllSnippetsWithTag,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, @params, splitOn: "LanguageId, FlagId");
                }
            }, null);
        }

        public IEnumerable<Snippet> GetAllSnippetsWithTitle(string _title)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        Title = _title
                    };
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetAllSnippetsWithTitle,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, @params, splitOn: "LanguageId, FlagId");
                }
            }, null);
        }

        public Snippet GetSnippet(Guid _snippetId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString()
                    };
                    return connection.Query<Snippet, Language, Flag, Snippet>(SqlDictionary.GetSnippet,
                        (_snippet, _language, _flag) =>
                        {
                            _snippet.Language = _language;
                            _snippet.Flag = _flag;
                            return _snippet;
                        }, @params, splitOn: "LanguageId, FlagId").SingleOrDefault();
                }
            }, null);
        }

        public bool ModifySnippet(Snippet _snippet)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippet.SnippetId.ToString(),
                        LanguageId = _snippet.Language.LanguageId,
                        FlagId = _snippet.Flag.FlagId,
                        Title = _snippet.Title,
                        SnippetDescription = _snippet.SnippetDescription,
                        Code = _snippet.Code,
                        CreationTime = _snippet.CreationTime,
                        LastEditTime = _snippet.LastEditTime
                    };
                    connection.Execute(SqlDictionary.ModifySnippet, @params);
                    return true;
                }
            }, false);
        }

        public bool SnippetExists(Guid _snippetId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString()
                    };
                    return connection.Query<bool>(SqlDictionary.SnippetExists, @params).Single();
                }
            }, false);
        }
    }
}