﻿using System;
using System.Collections.Generic;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class TagAssignmentLogic : ITagAssignmentLogic
    {
        private readonly ILog logger_;
        private readonly ISnippetRepository snippetRepository_;
        private readonly ITagAssignmentRepository tagAssignmentRepository_;
        private readonly ITagRepository tagRepository_;

        public TagAssignmentLogic(
            ILog _logger,
            ISnippetRepository _snippetRepository,
            ITagRepository _tagRepository,
            ITagAssignmentRepository _tagAssignmentRepository)
        {
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
            snippetRepository_ = _snippetRepository ?? throw new ArgumentException("SnippetRepository cannot be null!");
            tagRepository_ = _tagRepository ?? throw new ArgumentException("TagRepository cannot be null!");
            tagAssignmentRepository_ = _tagAssignmentRepository ?? throw new ArgumentException("TagAssignmentRepository cannot be null!");
        }

        public bool AssignTagToSnippet(Guid _snippetId, string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning false...");
                return false;
            }
            if (!snippetRepository_.SnippetExists(_snippetId))
            {
                logger_.Error("Snippet doesn't exist, returning false...");
                return false;
            }
            if (!tagRepository_.TagExists(_tagName))
            {
                tagRepository_.AddTag(_tagName);
            }

            var tag = tagRepository_.GetTag(_tagName);
            return tagAssignmentRepository_.AssignTagToSnippet(_snippetId, tag.TagId);
        }

        public bool DeleteAllAssignments()
        {
            return tagAssignmentRepository_.DeleteAllAssignments();
        }

        public IEnumerable<string> GetSnippetTags(Guid _snippetId)
        {
            return tagAssignmentRepository_.GetSnippetTags(_snippetId);
        }

        public bool TagAssignmentExists(Guid _snippetId, string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning false...");
                return false;
            }

            var tag = tagRepository_.GetTag(_tagName);
            return tagAssignmentRepository_.TagAssignmentExists(_snippetId, tag.TagId);
        }

        public bool UnassignTagFromSnippet(Guid _snippetId, string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning false...");
                return false;
            }
            var tag = tagRepository_.GetTag(_tagName);
            if (tag == null)
            {
                logger_.Error("Specified tag doesn't exist, returning false...");
                return false;
            }
            if (!tagAssignmentRepository_.TagAssignmentExists(_snippetId, tag.TagId))
            {
                logger_.Error("TagAssignment doesn't exist, returning false...");
                return false;
            }

            return tagAssignmentRepository_.UnassignTagFromSnippet(_snippetId, tag.TagId);
        }
    }
}