﻿using System;
using System.Data.SQLite;
using Dapper;
using log4net;

namespace Repository.Core
{
    public abstract class BaseRepository
    {
        protected readonly string ConnectionString;
        protected readonly ILog Logger;

        protected BaseRepository(string _connectionString, ILog _logger)
        {
            if (string.IsNullOrEmpty(_connectionString))
                throw new ArgumentException("Connection string cannot be null or empty!");
            ConnectionString = _connectionString;
            Logger = _logger ?? throw new ArgumentException("Logger cannot be null or empty!");
        }

        protected T ExecuteInLoggerTry<T>(Func<T> _function, T _faultResult)
        {
            try
            {
                return _function();
            }
            catch (Exception e)
            {
                Logger.Error("Error during SQL execution!", e);
                return _faultResult;
            }
        }
    }
}