﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class LanguageLogic : ILanguageLogic
    {
        private readonly ILanguageRepository languageRepository_;
        private readonly ILog logger_;

        public LanguageLogic(ILog _logger, ILanguageRepository _languageRepository)
        {
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
            languageRepository_ =
                _languageRepository ?? throw new ArgumentException("LanguageRepository cannot be null!");
        }

        public Language AddLanguage(string _languageName)
        {
            if (string.IsNullOrEmpty(_languageName))
            {
                logger_.Warn("LanguageName is null or empty, returning null...");
                return null;
            }
            languageRepository_.AddLanguage(_languageName);
            return languageRepository_.GetLanguage(_languageName);
        }

        public bool DeleteAllLanguages()
        {
            return languageRepository_.DeleteAllLanguages();
        }

        public bool DeleteLanguage(string _languageName)
        {
            if (string.IsNullOrEmpty(_languageName))
            {
                logger_.Warn("LanguageName is null or empty, returning false...");
                return false;
            }
            if (languageRepository_.LanguageExists(_languageName))
                return languageRepository_.DeleteLanguage(_languageName);
            logger_.Warn("Specified language doesn't exist, no language has been deleted.");
            return false;
        }

        public IEnumerable<string> GetAllLanguages()
        {
            return languageRepository_.GetAllLanguages();
        }

        public Language GetLanguage(string _languageName)
        {
            if (!string.IsNullOrEmpty(_languageName)) return languageRepository_.GetLanguage(_languageName);
            logger_.Warn("LanguageName is null or empty, returning null...");
            return null;
        }

        public bool LanguageExists(string _languageName)
        {
            if (!string.IsNullOrEmpty(_languageName)) return languageRepository_.LanguageExists(_languageName);
            logger_.Warn("LanguageName is null or empty, returning false...");
            return false;
        }

        public bool ModifyLanguage(Language _language)
        {
            if (_language != null) return languageRepository_.ModifyLanguage(_language);
            logger_.Warn("Language is null, returning false...");
            return false;
        }
    }
}