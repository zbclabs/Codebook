﻿using System;

namespace Api.Dto
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()

    public class Snippet : ICloneable
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        private string code_;
        private Flag flag_;
        private Language language_;
        private DateTime lastEditDate_;
        private string title_;

        internal Snippet()
        {
        }

        public string Code
        {
            get => code_;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Code cannot be null or empty!");
                code_ = value;
            }
        }

        public DateTime CreationTime { get; private set; }

        public Flag Flag
        {
            get => flag_;
            set => flag_ = value ?? throw new ArgumentException("Flag cannot be null!");
        }

        public Language Language
        {
            get => language_;
            set => language_ = value ?? throw new ArgumentException("Language cannot be null!");
        }

        public DateTime LastEditTime
        {
            get => lastEditDate_;
            set
            {
                if (value < CreationTime) throw new ArgumentException("Cannot set LastEditDate to date earlier than CreationDate");
                lastEditDate_ = value;
            }
        }

        public string SnippetDescription { get; set; }

        public Guid SnippetId { get; private set; }

        public string Title
        {
            get => title_;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Title cannot be null or empty!");
                title_ = value;
            }
        }

        public Snippet(Language _language, Flag _flag, string _title, string _code, string _snippetDescription = null)
        {
            SnippetId = Guid.NewGuid();
            Language = _language;
            Flag = _flag;
            Title = _title;
            Code = _code;
            SnippetDescription = _snippetDescription;
            CreationTime = DateTime.Now;
            LastEditTime = CreationTime;
        }

        public object Clone()
        {
            return new Snippet()
            {
                Code = Code,
                Flag = (Flag)Flag.Clone(),
                Language = (Language)Language.Clone(),
                LastEditTime = LastEditTime,
                SnippetDescription = SnippetDescription,
                Title = Title,
                SnippetId = SnippetId,
                CreationTime = CreationTime
            };
        }

        public override bool Equals(object _object)
        {
            if (_object is Snippet snippet)
            {
                return SnippetId.Equals(snippet.SnippetId)
                        && Language.Equals(snippet.Language)
                        && Flag.Equals(snippet.Flag)
                        && Title.Equals(snippet.Title)
                        && string.Equals(SnippetDescription, snippet.SnippetDescription)
                        && Code.Equals(snippet.Code)
                        && CreationTime.Equals(snippet.CreationTime)
                        && LastEditTime.Equals(snippet.LastEditTime);
            }
            return false;
        }
    }
}