﻿using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Logic
{
    public interface IFlagLogic
    {
        Flag AddFlag(string _flagName);

        bool DeleteAllFlags();

        bool DeleteFlag(string _flagName);

        bool FlagExists(string _flagName);

        IEnumerable<string> GetAllFlags();

        Flag GetFlag(string _flagName);

        bool ModifyFlag(Flag _flag);
    }
}