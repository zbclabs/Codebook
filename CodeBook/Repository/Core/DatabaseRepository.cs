﻿using System;
using System.Data.SQLite;
using System.IO;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class DatabaseRepository : BaseRepository, IDatabaseRepository
    {
        private readonly string databaseFilePath_;

        public DatabaseRepository(string _databaseFilePath, string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
            if (string.IsNullOrEmpty(_databaseFilePath)) throw new ArgumentException("DatabaseFilePath is null or empty!");
            databaseFilePath_ = _databaseFilePath;
        }

        public bool CreateDatabase()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString, true))
                {
                    connection.Execute(SqlDictionary.CreateLanguagesTable);
                    connection.Execute(SqlDictionary.CreateFlagsTable);
                    connection.Execute(SqlDictionary.CreateSnippetsTable);
                    connection.Execute(SqlDictionary.CreateTagsTable);
                    connection.Execute(SqlDictionary.CreateTagAssignmentsTable);
                    return true;
                }
            }, false);
        }

        public bool DropDatabase()
        {
            return ExecuteInLoggerTry(() =>
            {
                File.Delete(databaseFilePath_);
                return true;
            }, false);
        }
    }
}