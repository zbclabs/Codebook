﻿using System;
using System.IO;
using log4net;
using Repository.Core;

namespace Repository.Tests.Core
{
    public abstract class RepositoryTestBase
    {
        protected readonly string DatabaseFilePath = AppDomain.CurrentDomain.BaseDirectory + @"\CodeBook.db";
        protected string ConnectionString;
        protected DatabaseRepository DatabaseRepository;
        protected ILog Logger;

        public void SetUp()
        {
            log4net.Config.BasicConfigurator.Configure(
                new log4net.Appender.ConsoleAppender
                {
                    Name = "ConsoleLogger",
                    Layout = new log4net.Layout.SimpleLayout()
                });
            Logger = LogManager.GetLogger("ConsoleLogger");
            if (File.Exists(DatabaseFilePath)) File.Delete(DatabaseFilePath);
            ConnectionString = $"Data Source={DatabaseFilePath};Version=3";
            DatabaseRepository = new DatabaseRepository(DatabaseFilePath, ConnectionString, Logger);
            DatabaseRepository.CreateDatabase();
        }

        public void TearDown()
        {
            DatabaseRepository.DropDatabase();
        }
    }
}