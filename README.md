## Co to takiego?

Ile razy zdarzyło Ci się potrzebować fragmentu kodu, który napisałeś już wcześniej?
Zamiast szukać go w poprzednich projektach lub pisać na nowo, użyj CodeBooka!

CodeBook to podręczny zeszyt na wklejki, które mogą Ci się przydać w przyszłości.

## Dlaczego CodeBook?

![](https://zbclabs.dev/wp-content/uploads/2017/11/list.png)
&nbsp;&nbsp;Organizuj swoje wklejki według własnych preferencji

![](https://zbclabs.dev/wp-content/uploads/2017/11/smiling.png)
&nbsp;&nbsp;Zdaj się na intuicyjny interfejs

![](https://zbclabs.dev/wp-content/uploads/2017/11/settings.png)
&nbsp;&nbsp;Dostosuj go do siebie

![](https://zbclabs.dev/wp-content/uploads/2017/11/open.png)
&nbsp;&nbsp;Jest projektem Open Source

## Przyspiesz swoją pracę z CodeBookiem

#### **Trzy kategorie: flagi, języki i tagi**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot1.png)

#### **Wyszukiwanie po wszystkim – nawet dacie!**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot2.png)

#### **Nie ma żadnych sztywnych reguł – twórz własne flagi, języki i tagi**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot3.png)

#### **Edytuj stare wklejki, wprowadzaj poprawki**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot4.png)

#### **Twórz nowe, nic prostszego!**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot5.png)

#### **Wybierz swoje kolory – jasny / ciemny + kolor akcentu**
![](https://zbclabs.dev/wp-content/uploads/2017/11/Screenshot6.png)

## Open Source

CodeBook jest projektem open source – od programistów, dla programistów!

Jeśli potrzebujesz, śmiało wprowadzaj modyfikacje albo daj nam znać o swoim pomyśle – chętnie wprowadzimy go w następnej wersji.

## Wymagania systemowe:

Procesor 64-bit
.NET Framework 4.6.1

Wszystkie ikony poza logo CodeBooka dzięki FlatIcon
[https://www.flaticon.com/](https://www.flaticon.com/)
