﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Repository;
using log4net;
using Logic.Core;
using Moq;
using NUnit.Framework;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class SnippetLogicTests
    {
        private Mock<IFlagRepository> flagRepositoryMock_;
        private Mock<ILanguageRepository> languageRepositoryMock_;
        private Mock<ILog> loggerMock_;
        private SnippetLogic snippetLogic_;
        private Mock<ISnippetRepository> snippetRepositoryMock_;
        private Mock<ITagRepository> tagRepositoryMock_;

        [Test, Category("Unit")]
        public void AddSnippet_FlagDoesntExist()
        {
            var flag = "Done";
            var snippet = new Snippet(new Language("C#"), new Flag(flag), "Title", "nope();");
            flagRepositoryMock_.Setup(_x => _x.FlagExists(flag)).Returns(false);
            flagRepositoryMock_.Setup(_x => _x.AddFlag(flag)).Returns(true);
            flagRepositoryMock_.Setup(_x => _x.GetFlag(flag)).Returns(new Flag(flag));
            languageRepositoryMock_.Setup(_x => _x.LanguageExists(It.IsAny<string>())).Returns(true);
            snippetLogic_.AddSnippet(snippet);
            languageRepositoryMock_.Verify(_x => _x.AddLanguage(It.IsAny<string>()), Times.Never);
            flagRepositoryMock_.Verify(_x => _x.AddFlag(flag), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.AddSnippet(snippet), Times.Once);
        }

        [Test, Category("Unit")]
        public void AddSnippet_LanguageDoesntExist()
        {
            var language = "C#";
            var snippet = new Snippet(new Language(language), new Flag("Done"), "Title", "nope();");
            languageRepositoryMock_.Setup(_x => _x.LanguageExists(language)).Returns(false);
            languageRepositoryMock_.Setup(_x => _x.AddLanguage(language)).Returns(true);
            languageRepositoryMock_.Setup(_x => _x.GetLanguage(language)).Returns(new Language(language));
            flagRepositoryMock_.Setup(_x => _x.FlagExists(It.IsAny<string>())).Returns(true);
            snippetLogic_.AddSnippet(snippet);
            languageRepositoryMock_.Verify(_x => _x.AddLanguage(language), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.AddFlag(It.IsAny<string>()), Times.Never);
            snippetRepositoryMock_.Verify(_x => _x.AddSnippet(snippet), Times.Once);
        }

        [Test, Category("Unit")]
        public void AddSnippet_SnippetIsNull()
        {
            Assert.AreEqual(false, snippetLogic_.AddSnippet(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.AddSnippet(It.IsAny<Snippet>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteAllSnippets()
        {
            snippetRepositoryMock_.Setup(_x => _x.DeleteAllSnippets()).Returns(true);
            Assert.AreEqual(true, snippetLogic_.DeleteAllSnippets());
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            snippetRepositoryMock_.Verify(_x => _x.DeleteAllSnippets(), Times.Once);
        }

        [Test, Category("Unit")]
        public void DeleteSnippet_SnippetDoesntExist()
        {
            var snippetId = Guid.NewGuid();
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippetId)).Returns(false);
            Assert.AreEqual(false, snippetLogic_.DeleteSnippet(snippetId));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.DeleteSnippet(It.IsAny<Guid>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteSnippet_SnippetExists()
        {
            var snippetId = Guid.NewGuid();
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippetId)).Returns(true);
            snippetRepositoryMock_.Setup(_x => _x.DeleteSnippet(snippetId)).Returns(true);
            Assert.AreEqual(true, snippetLogic_.DeleteSnippet(snippetId));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            snippetRepositoryMock_.Verify(_x => _x.DeleteSnippet(snippetId), Times.Once);
        }

        [Test, Category("Unit")]
        public void GetAllMatchingSnippets_PredicateIsCorrect()
        {
            var language1 = new Language("C#");
            var language2 = new Language("Python");
            var flag1 = new Flag("Done");
            var flag2 = new Flag("InProgress");
            var title = "Booya!";

            var snippet1 = new Snippet(language1, flag1, "Title", "nope();");
            var snippet2 = new Snippet(language1, flag2, "Booya!", "minion();", "Boooooooooya!");
            var snippet3 = new Snippet(language2, flag1, "Booya!", "minion();", "Boooya, but in Python!");
            var snippet4 = new Snippet(language2, flag2, "Anything", "meh..", "Nope...");

            var snippets = new List<Snippet>() { snippet1, snippet2, snippet3, snippet4 };

            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippets()).Returns(snippets);
            var filteredSnippets = snippetLogic_.GetAllMatchingSnippets(_x =>
                _x.Language.LanguageName == language1.LanguageName || _x.Title == title).ToList();

            Assert.AreEqual(true, filteredSnippets.Contains(snippet1));
            Assert.AreEqual(true, filteredSnippets.Contains(snippet2));
            Assert.AreEqual(true, filteredSnippets.Contains(snippet3));
            Assert.AreEqual(false, filteredSnippets.Contains(snippet4));
        }

        [Test, Category("Unit")]
        public void GetAllMatchingSnippets_PredicateIsNull()
        {
            Assert.AreEqual(null, snippetLogic_.GetAllMatchingSnippets(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.GetAllSnippets(), Times.Never);
        }

        [Test, Category("Unit")]
        public void GetAllSnippets()
        {
            var snippet1 = new Snippet(new Language("C#"), new Flag("Done"), "Another f*****g snippet", "fuckOff();");
            var snippet2 = new Snippet(new Language("Brainfuck"), new Flag("WTF?"), "I don't know what is this", "r0j2ni0engi0", "Whaaaat?");

            var snippets = new List<Snippet>() { snippet1, snippet2 };
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippets()).Returns(snippets);
            var readSnippets = snippetLogic_.GetAllSnippets().ToList();

            Assert.AreEqual(true, readSnippets.Contains(snippet1));
            Assert.AreEqual(true, readSnippets.Contains(snippet2));
        }

        [Test, Category("Unit")]
        public void GetAllSnippetsWithFlag_FlagIsCorrect()
        {
            var flagName = "Done";
            var flag = new Flag(flagName);
            var snippet = new Snippet(new Language("C#"), flag, "Nope", "ijfeiwn");
            var snippets = new List<Snippet> { snippet };
            flagRepositoryMock_.Setup(_x => _x.GetFlag(flagName)).Returns(flag);
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithFlag(flag.FlagId)).Returns(snippets);
            var readSnippets = snippetLogic_.GetAllSnippetsWithFlag(flagName);
            Assert.AreEqual(true, readSnippets.Contains(snippet));
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetAllSnippetsWithFlag_FlagIsNullOrEmpty(string _flagName)
        {
            Assert.AreEqual(null, snippetLogic_.GetAllSnippetsWithFlag(_flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.GetAllSnippetsWithFlag(It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void GetAllSnippetsWithLanguage_LanguageIsCorrect()
        {
            var languageName = "C#";
            var language = new Language(languageName);
            var snippet = new Snippet(language, new Flag("Done"), "Nope", "ijfeiwn");
            var snippets = new List<Snippet> { snippet };
            languageRepositoryMock_.Setup(_x => _x.GetLanguage(languageName)).Returns(language);
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithLanguage(language.LanguageId)).Returns(snippets);
            var readSnippets = snippetLogic_.GetAllSnippetsWithLanguage(languageName);
            Assert.AreEqual(true, readSnippets.Contains(snippet));
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetAllSnippetsWithLanguage_LanguageIsNullOrEmpty(string _languageName)
        {
            Assert.AreEqual(null, snippetLogic_.GetAllSnippetsWithLanguage(_languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.GetAllSnippetsWithLanguage(It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void GetAllSnippetsWithTag_TagIsCorrect()
        {
            var tagName = "List";
            var tag = new Tag(tagName);
            var snippet = new Snippet(new Language("C#"), new Flag("Done"), "Nope", "ijfeiwn");
            var snippets = new List<Snippet> { snippet };

            tagRepositoryMock_.Setup(_x => _x.GetTag(tagName)).Returns(tag);
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithTag(tag.TagId)).Returns(snippets);
            var readSnippets = snippetLogic_.GetAllSnippetsWithTag(tagName);
            Assert.AreEqual(true, readSnippets.Contains(snippet));
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetAllSnippetsWithTag_TagIsNullOrEmpty(string _tagName)
        {
            Assert.AreEqual(null, snippetLogic_.GetAllSnippetsWithTag(_tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.GetAllSnippetsWithTag(It.IsAny<int>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void GetAllSnippetsWithTitle_TitleIsCorrect()
        {
            var title = "MySnippet";
            var snippet = new Snippet(new Language("C#"), new Flag("Done"), title, "ijfeiwn");
            var snippets = new List<Snippet> { snippet };
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithTitle(title)).Returns(snippets);
            var readSnippets = snippetLogic_.GetAllSnippetsWithTitle(title);
            Assert.AreEqual(true, readSnippets.Contains(snippet));
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetAllSnippetsWithTitle_TitleIsNullOrEmpty(string _titleName)
        {
            Assert.AreEqual(null, snippetLogic_.GetAllSnippetsWithTitle(_titleName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.GetAllSnippetsWithTitle(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void ModifySnippet_SnippetDoesntExist()
        {
            var snippet = new Snippet(new Language("C#"), new Flag("Lol"), "Rotfl", "gtfo();");
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippet.SnippetId)).Returns(false);
            Assert.AreEqual(false, snippetLogic_.ModifySnippet(snippet));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.ModifySnippet(snippet), Times.Never);
        }

        [Test, Category("Unit")]
        public void ModifySnippet_SnippetExists()
        {
            var snippet = new Snippet(new Language("C#"), new Flag("Lol"), "Rotfl", "gtfo();");
            flagRepositoryMock_.Setup(_x => _x.FlagExists("Lol")).Returns(true);
            languageRepositoryMock_.Setup(_x => _x.LanguageExists("C#")).Returns(true);
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippet.SnippetId)).Returns(true);
            Assert.AreEqual(false, snippetLogic_.ModifySnippet(snippet));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            snippetRepositoryMock_.Verify(_x => _x.ModifySnippet(snippet), Times.Once);
        }

        [Test, Category("Unit")]
        public void ModifySnippet_SnippetIsNull()
        {
            Assert.AreEqual(false, snippetLogic_.ModifySnippet(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            snippetRepositoryMock_.Verify(_x => _x.ModifySnippet(It.IsAny<Snippet>()), Times.Never);
        }

        [SetUp]
        public void SetUp()
        {
            loggerMock_ = new Mock<ILog>();
            loggerMock_.Setup(_x => _x.Info(It.IsAny<string>()));
            loggerMock_.Setup(_x => _x.Warn(It.IsAny<string>()));

            flagRepositoryMock_ = new Mock<IFlagRepository>();
            languageRepositoryMock_ = new Mock<ILanguageRepository>();
            snippetRepositoryMock_ = new Mock<ISnippetRepository>();
            tagRepositoryMock_ = new Mock<ITagRepository>();

            snippetLogic_ = new SnippetLogic(
                loggerMock_.Object,
                languageRepositoryMock_.Object,
                flagRepositoryMock_.Object,
                snippetRepositoryMock_.Object,
                tagRepositoryMock_.Object);
        }

        [Test, Category("Unit")]
        [TestCase(true)]
        [TestCase(false)]
        public void SnippetExists(bool _snippetExists)
        {
            var snippetId = Guid.NewGuid();
            snippetRepositoryMock_.Setup(_x => _x.SnippetExists(snippetId)).Returns(_snippetExists);
            Assert.AreEqual(_snippetExists, snippetLogic_.SnippetExists(snippetId));
        }

        [Test, Category("Unit")]
        [TestCase(true, false, false, false, false)]
        [TestCase(false, true, false, false, false)]
        [TestCase(false, false, true, false, false)]
        [TestCase(false, false, false, true, false)]
        [TestCase(false, false, false, false, true)]
        public void SnippetLogicConstructor_IncorrectArguments(
            bool _isLoggerNull,
            bool _isLanguageRepositoryNull,
            bool _isFlagRepositoryNull,
            bool _isSnippetRepositoryNull,
            bool _isTagRepositoryNull)
        {
            var logger = _isLoggerNull ? null : loggerMock_.Object;
            var languageRepository = _isLanguageRepositoryNull ? null : languageRepositoryMock_.Object;
            var flagRepository = _isFlagRepositoryNull ? null : flagRepositoryMock_.Object;
            var snippetRepository = _isSnippetRepositoryNull ? null : snippetRepositoryMock_.Object;
            var tagRepository = _isTagRepositoryNull ? null : tagRepositoryMock_.Object;

            try
            {
                var snippetLogic = new SnippetLogic(logger, languageRepository, flagRepository, snippetRepository, tagRepository);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
        }
    }
}