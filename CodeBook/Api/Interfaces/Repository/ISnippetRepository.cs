﻿using System;
using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Repository
{
    public interface ISnippetRepository
    {
        bool AddSnippet(Snippet _snippet);

        bool DeleteAllSnippets();

        bool DeleteSnippet(Guid _snippetId);

        IEnumerable<Snippet> GetAllSnippets();

        IEnumerable<Snippet> GetAllSnippetsWithFlag(int _flagId);

        IEnumerable<Snippet> GetAllSnippetsWithLanguage(int _languageId);

        IEnumerable<Snippet> GetAllSnippetsWithTag(int _tagId);

        IEnumerable<Snippet> GetAllSnippetsWithTitle(string _title);

        Snippet GetSnippet(Guid _snippetId);

        bool ModifySnippet(Snippet _snippet);

        bool SnippetExists(Guid _snippetId);
    }
}