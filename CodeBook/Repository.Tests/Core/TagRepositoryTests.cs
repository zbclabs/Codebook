﻿using System.Linq;
using NUnit.Framework;
using Repository.Core;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class TagRepositoryTests : RepositoryTestBase
    {
        private TagRepository tagRepository_;

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            tagRepository_ = new TagRepository(ConnectionString, Logger);
        }

        [Test, Category("Integration")]
        public void TagRepositoryTest()
        {
            // Tagi jeszcze nie istnieją - te metody powinny zwrócić false lub null
            Assert.That(tagRepository_.TagExists("Tag"), Is.EqualTo(false));
            Assert.That(tagRepository_.GetAllTags().ToList().Count, Is.EqualTo(0));
            Assert.That(tagRepository_.GetTag("Flag"), Is.EqualTo(null));

            // Utworzenie tagów - te metody powinny przejść
            var tagName1 = "List";
            var tagName2 = "Adapter";
            Assert.That(tagRepository_.AddTag(tagName1), Is.EqualTo(true));
            Assert.That(tagRepository_.AddTag(tagName2), Is.EqualTo(true));

            // Utworzenie duplikatu - ta metoda powinna nawalić
            Assert.That(tagRepository_.AddTag(tagName1), Is.EqualTo(false));

            // Odczyt wszystkich tagów
            var tag1 = tagRepository_.GetTag(tagName1);
            Assert.AreNotEqual(null, tag1);
            var tag2 = tagRepository_.GetTag(tagName2);
            Assert.AreNotEqual(null, tag2);
            var allFlags = tagRepository_.GetAllTags().ToList();
            Assert.That(allFlags.Contains(tagName1), Is.EqualTo(true));
            Assert.That(allFlags.Contains(tagName2), Is.EqualTo(true));

            // Sprawdzenie, czy tagi istnieją
            Assert.That(tagRepository_.TagExists(tagName1), Is.EqualTo(true));
            Assert.That(tagRepository_.TagExists(tagName2), Is.EqualTo(true));
            Assert.That(tagRepository_.TagExists("Nope"), Is.EqualTo(false));

            // Modyfikacja tagu
            tag1.TagName = "Trash";
            Assert.That(tagRepository_.ModifyTag(tag1), Is.EqualTo(true));
            Assert.That(tagRepository_.GetTag(tag1.TagName), Is.EqualTo(tag1));

            // Usunięcie pojedynczego tagu
            Assert.That(tagRepository_.DeleteTag(tag1.TagName), Is.EqualTo(true));
            Assert.That(tagRepository_.TagExists(tag1.TagName), Is.EqualTo(false));

            // Usunięcie wszystkich tagów
            Assert.That(tagRepository_.DeleteAllTags, Is.EqualTo(true));
            Assert.That(tagRepository_.GetAllTags().ToList().Count, Is.EqualTo(0));
        }

        [TearDown]
        public new void TearDown()
        {
            base.TearDown();
        }
    }
}