﻿using MahApps.Metro.Controls;
using UserExperience.ViewModel;

namespace UserExperience.Views
{
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }
    }
}