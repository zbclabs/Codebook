﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class FlagRepository : BaseRepository, IFlagRepository
    {
        public FlagRepository(string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
        }

        public bool AddFlag(string _flagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagName = _flagName
                    };
                    connection.Execute(SqlDictionary.AddFlag, @params);
                    return true;
                }
            }, false);
        }

        public bool DeleteAllFlags()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    connection.Execute(SqlDictionary.DeleteAllFlags);
                    return true;
                }
            }, false);
        }

        public bool DeleteFlag(string _flagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagName = _flagName
                    };
                    connection.Execute(SqlDictionary.DeleteFlag, @params);
                    return true;
                }
            }, false);
        }

        public bool FlagExists(string _flagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagName = _flagName
                    };
                    return connection.Query<bool>(SqlDictionary.FlagExists, @params).Single();
                }
            }, false);
        }

        public IEnumerable<string> GetAllFlags()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    return connection.Query<string>(SqlDictionary.GetAllFlags);
                }
            }, null);
        }

        public Flag GetFlag(string _flagName)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagName = _flagName
                    };
                    return connection.Query<Flag>(SqlDictionary.GetFlag, @params).SingleOrDefault();
                }
            }, null);
        }

        public bool ModifyFlag(Flag _flag)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        FlagId = _flag.FlagId,
                        FlagName = _flag.FlagName
                    };
                    connection.Execute(SqlDictionary.ModifyFlag, @params);
                    return true;
                }
            }, false);
        }
    }
}