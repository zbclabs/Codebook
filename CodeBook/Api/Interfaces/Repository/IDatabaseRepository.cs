﻿namespace Api.Interfaces.Repository
{
    public interface IDatabaseRepository
    {
        bool CreateDatabase();

        bool DropDatabase();
    }
}