﻿using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Repository;
using Logic.Core;
using Moq;
using NUnit.Framework;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class DatabaseLogicTests
    {
        private DatabaseLogic databaseLogic_;
        private Mock<IDatabaseRepository> databaseRepositoryMock_;
        private Mock<IFlagRepository> flagRepositoryMock_;
        private Mock<ILanguageRepository> languageRepositoryMock_;
        private Mock<ISnippetRepository> snippetRepositoryMock_;
        private Mock<ITagRepository> tagRepositoryMock_;

        [Test, Category("Unit")]
        public void CleanUpDatabase()
        {
            var flag1 = new Flag { FlagId = 1, FlagName = "Done" };
            var flag2 = new Flag { FlagId = 2, FlagName = "Prototype" };
            flagRepositoryMock_.Setup(_x => _x.GetFlag(flag1.FlagName)).Returns(flag1);
            flagRepositoryMock_.Setup(_x => _x.GetFlag(flag2.FlagName)).Returns(flag2);
            flagRepositoryMock_.Setup(_x => _x.GetAllFlags())
                .Returns(new List<string> { flag1.FlagName, flag2.FlagName });
            flagRepositoryMock_.Setup(_x => _x.DeleteFlag(It.IsAny<string>())).Returns(true);

            var language1 = new Language { LanguageId = 1, LanguageName = "C#" };
            var language2 = new Language { LanguageId = 2, LanguageName = "Python" };
            languageRepositoryMock_.Setup(_x => _x.GetLanguage(language1.LanguageName)).Returns(language1);
            languageRepositoryMock_.Setup(_x => _x.GetLanguage(language2.LanguageName)).Returns(language2);
            languageRepositoryMock_.Setup(_x => _x.GetAllLanguages())
                .Returns(new List<string> { language1.LanguageName, language2.LanguageName });
            languageRepositoryMock_.Setup(_x => _x.DeleteLanguage(It.IsAny<string>())).Returns(true);

            var tag1 = new Tag { TagId = 1, TagName = "Xamarin" };
            var tag2 = new Tag { TagId = 2, TagName = "XAML" };
            tagRepositoryMock_.Setup(_x => _x.GetTag(tag1.TagName)).Returns(tag1);
            tagRepositoryMock_.Setup(_x => _x.GetTag(tag2.TagName)).Returns(tag2);
            tagRepositoryMock_.Setup(_x => _x.GetAllTags()).Returns(new List<string> { tag1.TagName, tag2.TagName });
            tagRepositoryMock_.Setup(_x => _x.DeleteTag(It.IsAny<string>())).Returns(true);

            var snippet = new Snippet(language1, flag1, ".NET", "<lol/>");
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithFlag(flag1.FlagId))
                .Returns(new List<Snippet> { snippet });
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithLanguage(language1.LanguageId))
                .Returns(new List<Snippet> { snippet });
            snippetRepositoryMock_.Setup(_x => _x.GetAllSnippetsWithTag(tag1.TagId))
                .Returns(new List<Snippet> { snippet });

            Assert.That(databaseLogic_.CleanUpDatabase(), Is.EqualTo(true));
            flagRepositoryMock_.Verify(_x => _x.DeleteFlag(flag1.FlagName), Times.Never);
            flagRepositoryMock_.Verify(_x => _x.DeleteFlag(flag2.FlagName), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.DeleteLanguage(language1.LanguageName), Times.Never);
            languageRepositoryMock_.Verify(_x => _x.DeleteLanguage(language2.LanguageName), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.DeleteTag(tag1.TagName), Times.Never);
            tagRepositoryMock_.Verify(_x => _x.DeleteTag(tag2.TagName), Times.Once);
        }

        [Test, Category("Unit")]
        public void CreateDatabase()
        {
            databaseRepositoryMock_.Setup(_x => _x.CreateDatabase()).Returns(true);
            Assert.That(databaseLogic_.CreateDatabase(), Is.EqualTo(true));
        }

        [Test, Category("Unit")]
        public void DropDatabase()
        {
            databaseRepositoryMock_.Setup(_x => _x.DropDatabase()).Returns(true);
            Assert.That(databaseLogic_.DropDatabase(), Is.EqualTo(true));
        }

        [SetUp]
        public void SetUp()
        {
            databaseRepositoryMock_ = new Mock<IDatabaseRepository>();
            flagRepositoryMock_ = new Mock<IFlagRepository>();
            languageRepositoryMock_ = new Mock<ILanguageRepository>();
            snippetRepositoryMock_ = new Mock<ISnippetRepository>();
            tagRepositoryMock_ = new Mock<ITagRepository>();

            databaseLogic_ = new DatabaseLogic(
                databaseRepositoryMock_.Object,
                flagRepositoryMock_.Object,
                languageRepositoryMock_.Object,
                snippetRepositoryMock_.Object,
                tagRepositoryMock_.Object);
        }
    }
}