﻿using System;

namespace Api.Dto
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()

    public class Tag
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        private string tagName_;

        internal Tag()
        {
        }

        public int TagId { get; internal set; }

        public string TagName
        {
            get => tagName_;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("TagName cannot be null or empty!");
                tagName_ = value;
            }
        }

        public Tag(string _tagName)
        {
            TagId = -1;
            TagName = _tagName;
        }

        public override bool Equals(object _object)
        {
            if (_object is Tag tag)
            {
                return TagId.Equals(tag.TagId)
                        && TagName.Equals(tag.TagName);
            }
            return false;
        }
    }
}