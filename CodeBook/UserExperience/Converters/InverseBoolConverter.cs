﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UserExperience.Converters
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBoolConverter : IValueConverter
    {
        private bool Invert(object _value, Type _targetType)
        {
            if (_targetType != typeof(bool)) throw new InvalidOperationException("Target must be bool.");
            return !(bool)_value;
        }

        public object Convert(object _value, Type _targetType, object _parameter, CultureInfo _culture)
        {
            return Invert(_value, _targetType);
        }

        public object ConvertBack(object _value, Type _targetType, object _parameter, CultureInfo _culture)
        {
            return Invert(_value, _targetType);
        }
    }
}