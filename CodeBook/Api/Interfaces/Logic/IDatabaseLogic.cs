﻿namespace Api.Interfaces.Logic
{
    public interface IDatabaseLogic
    {
        bool CleanUpDatabase();

        bool CreateDatabase();

        bool DropDatabase();
    }
}