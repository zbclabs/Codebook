﻿using System;
using Api.Interfaces.Repository;
using log4net;
using Ninject.Modules;
using Repository.Core;

namespace Logic.Ninject
{
    public class RepositoryModule : NinjectModule
    {
        private readonly string connectionString_;
        private readonly string databaseFilePath_;
        private readonly ILog logger_;

        public RepositoryModule(string _databaseFilePath, ILog _logger)
        {
            if (string.IsNullOrEmpty(_databaseFilePath)) throw new ArgumentException("DatabaseFilePath cannot be null or empty!");
            databaseFilePath_ = _databaseFilePath;
            connectionString_ = $"Data Source={_databaseFilePath}; Version=3";
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
        }

        public override void Load()
        {
            Bind<IDatabaseRepository>()
                .To<DatabaseRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_databaseFilePath", databaseFilePath_)
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);

            Bind<ILanguageRepository>()
                .To<LanguageRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);

            Bind<IFlagRepository>()
                .To<FlagRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);

            Bind<ITagRepository>()
                .To<TagRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);

            Bind<ISnippetRepository>()
                .To<SnippetRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);

            Bind<ITagAssignmentRepository>()
                .To<TagAssignmentRepository>()
                .InSingletonScope()
                .WithConstructorArgument("_connectionString", connectionString_)
                .WithConstructorArgument("_logger", logger_);
        }
    }
}