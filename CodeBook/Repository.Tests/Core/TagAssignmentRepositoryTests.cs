﻿using System.Linq;
using Api.Dto;
using NUnit.Framework;
using Repository.Core;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class TagAssignmentRepositoryTests : RepositoryTestBase
    {
        private FlagRepository flagRepository_;
        private LanguageRepository languageRepository_;
        private SnippetRepository snippetRepository_;
        private TagAssignmentRepository tagAssignmentRepository_;
        private TagRepository tagRepository_;

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            flagRepository_ = new FlagRepository(ConnectionString, Logger);
            languageRepository_ = new LanguageRepository(ConnectionString, Logger);
            snippetRepository_ = new SnippetRepository(ConnectionString, Logger);
            tagAssignmentRepository_ = new TagAssignmentRepository(ConnectionString, Logger);
            tagRepository_ = new TagRepository(ConnectionString, Logger);
        }

        [Test, Category("Integration")]
        public void TagAssignmentRepositoryTest()
        {
            // Dodanie flagi
            Flag flag;
            {
                var flagName = "Done";
                flagRepository_.AddFlag(flagName);
                flag = flagRepository_.GetFlag(flagName);
            }
            // Dodanie języka
            Language language;
            {
                var languageName = "C#";
                languageRepository_.AddLanguage(languageName);
                language = languageRepository_.GetLanguage(languageName);
            }
            // Dodanie tagów
            Tag tag1;
            {
                var tagName = "Adapter";
                tagRepository_.AddTag(tagName);
                tag1 = tagRepository_.GetTag(tagName);
            }
            Tag tag2;
            {
                var tagName = "Lista";
                tagRepository_.AddTag(tagName);
                tag2 = tagRepository_.GetTag(tagName);
            }

            // Dodanie wklejki
            var snippet1 = new Snippet(language, flag, "Snippet1", "meow();");
            var snippet2 = new Snippet(language, flag, "Snippet2", "boom();");
            snippetRepository_.AddSnippet(snippet1);
            snippetRepository_.AddSnippet(snippet2);

            // Przypisań tagów jeszcze nie ma
            Assert.That(tagAssignmentRepository_.GetSnippetTags(snippet1.SnippetId).ToList().Count, Is.EqualTo(0));
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet1.SnippetId, tag1.TagId), Is.EqualTo(false));

            // Przypisanie tagów do wklejek
            Assert.That(tagAssignmentRepository_.AssignTagToSnippet(snippet1.SnippetId, tag1.TagId), Is.EqualTo(true));
            Assert.That(tagAssignmentRepository_.AssignTagToSnippet(snippet2.SnippetId, tag2.TagId), Is.EqualTo(true));

            // Duplikat
            Assert.That(tagAssignmentRepository_.AssignTagToSnippet(snippet1.SnippetId, tag1.TagId), Is.EqualTo(false));

            // Sprawdzenie, czy przypisania istnieją
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet1.SnippetId, tag1.TagId), Is.EqualTo(true));
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet1.SnippetId, tag2.TagId), Is.EqualTo(false));
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet2.SnippetId, tag1.TagId), Is.EqualTo(false));
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet2.SnippetId, tag2.TagId), Is.EqualTo(true));

            // Pobranie tagów wklejki
            Assert.That(tagAssignmentRepository_.GetSnippetTags(snippet1.SnippetId).Contains(tag1.TagName), Is.EqualTo(true));

            // Odpisanie flagi od wklejki
            Assert.That(tagAssignmentRepository_.UnassignTagFromSnippet(snippet1.SnippetId, tag1.TagId), Is.EqualTo(true));
            Assert.That(tagAssignmentRepository_.TagAssignmentExists(snippet1.SnippetId, tag1.TagId), Is.EqualTo(false));

            // Usunięcie wszystkich przypisań
            Assert.That(tagAssignmentRepository_.DeleteAllAssignments(), Is.EqualTo(true));
            Assert.That(tagAssignmentRepository_.GetSnippetTags(snippet1.SnippetId).ToList().Count, Is.EqualTo(0));
            Assert.That(tagAssignmentRepository_.GetSnippetTags(snippet2.SnippetId).ToList().Count, Is.EqualTo(0));
        }

        [TearDown]
        public new void TearDown()
        {
            base.TearDown();
        }
    }
}