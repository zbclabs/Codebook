﻿namespace Api.Interfaces.Logic
{
    public interface IApplicationLogic
    {
        IDatabaseLogic DatabaseLogic { get; }
        IFlagLogic FlagLogic { get; }
        ILanguageLogic LanguageLogic { get; }
        ISnippetLogic SnippetLogic { get; }
        ITagAssignmentLogic TagAssignmentLogic { get; }
        ITagLogic TagLogic { get; }
    }
}