﻿using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Repository
{
    public interface ITagRepository
    {
        bool AddTag(string _tagName);

        bool DeleteAllTags();

        bool DeleteTag(string _tagName);

        IEnumerable<string> GetAllTags();

        Tag GetTag(string _tagName);

        bool ModifyTag(Tag _tag);

        bool TagExists(string _tagName);
    }
}