﻿using System.Linq;
using Api.Dto;
using NUnit.Framework;
using Repository.Core;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class SnippetRepositoryTests : RepositoryTestBase
    {
        private FlagRepository flagRepository_;
        private LanguageRepository languageRepository_;
        private SnippetRepository snippetRepository_;
        private TagAssignmentRepository tagAssignmentRepository_;
        private TagRepository tagRepository_;

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            flagRepository_ = new FlagRepository(ConnectionString, Logger);
            languageRepository_ = new LanguageRepository(ConnectionString, Logger);
            snippetRepository_ = new SnippetRepository(ConnectionString, Logger);
            tagAssignmentRepository_ = new TagAssignmentRepository(ConnectionString, Logger);
            tagRepository_ = new TagRepository(ConnectionString, Logger);
        }

        [Test, Category("Integration")]
        public void SnippetRepositoryTest()
        {
            // Wklejki jeszcze nie istnieją
            Assert.That(snippetRepository_.GetAllSnippets().ToList().Count, Is.EqualTo(0));

            // Dodawanie języków
            Language language1;
            {
                var languageName = "C#";
                languageRepository_.AddLanguage(languageName);
                language1 = languageRepository_.GetLanguage(languageName);
            }
            Language language2;
            {
                var languageName = "VB.NET";
                languageRepository_.AddLanguage(languageName);
                language2 = languageRepository_.GetLanguage(languageName);
            }

            // Dodanie flag
            Flag flag1;
            {
                var flagName = "Done";
                flagRepository_.AddFlag(flagName);
                flag1 = flagRepository_.GetFlag(flagName);
            }
            Flag flag2;
            {
                var flagName = "InProgress";
                flagRepository_.AddFlag(flagName);
                flag2 = flagRepository_.GetFlag(flagName);
            }

            // Dodanie tagów
            Tag tag1;
            {
                var tagName = "List";
                tagRepository_.AddTag(tagName);
                tag1 = tagRepository_.GetTag(tagName);
            }
            Tag tag2;
            {
                var tagName = "Adapter";
                tagRepository_.AddTag(tagName);
                tag2 = tagRepository_.GetTag(tagName);
            }

            // Dodanie wklejek
            var title = "Snippet";
            var snippet1 = new Snippet(language1, flag1, title, "boo();");
            var snippet2 = new Snippet(language1, flag2, title, "boo();");
            var snippet3 = new Snippet(language2, flag1, title, "boo();");
            var snippet4 = new Snippet(language2, flag2, "Another", "boo();");
            Assert.That(snippetRepository_.AddSnippet(snippet1), Is.EqualTo(true));
            Assert.That(snippetRepository_.AddSnippet(snippet2), Is.EqualTo(true));
            Assert.That(snippetRepository_.AddSnippet(snippet3), Is.EqualTo(true));
            Assert.That(snippetRepository_.AddSnippet(snippet4), Is.EqualTo(true));

            // Dodanie duplikatu
            Assert.That(snippetRepository_.AddSnippet(snippet1), Is.EqualTo(false));

            // Przypisanie tagów
            tagAssignmentRepository_.AssignTagToSnippet(snippet1.SnippetId, tag1.TagId);
            tagAssignmentRepository_.AssignTagToSnippet(snippet2.SnippetId, tag1.TagId);
            tagAssignmentRepository_.AssignTagToSnippet(snippet3.SnippetId, tag2.TagId);
            tagAssignmentRepository_.AssignTagToSnippet(snippet4.SnippetId, tag2.TagId);

            // Sprawdzenie, czy wklejki istnieją
            Assert.That(snippetRepository_.SnippetExists(snippet1.SnippetId), Is.EqualTo(true));
            Assert.That(snippetRepository_.SnippetExists(snippet2.SnippetId), Is.EqualTo(true));

            // Pobieranie wszystkich wklejek
            Assert.That(snippetRepository_.GetAllSnippets().ToList().Count, Is.EqualTo(4));

            // Pobieranie wklejek z flagą
            var snippetsWithFlag = snippetRepository_.GetAllSnippetsWithFlag(flag1.FlagId).ToList();
            Assert.That(snippetsWithFlag.Contains(snippet1), Is.EqualTo(true));
            Assert.That(snippetsWithFlag.Contains(snippet2), Is.EqualTo(false));
            Assert.That(snippetsWithFlag.Contains(snippet3), Is.EqualTo(true));
            Assert.That(snippetsWithFlag.Contains(snippet4), Is.EqualTo(false));

            // Pobieranie wklejek z językiem
            var snippetsWithLanguage = snippetRepository_.GetAllSnippetsWithLanguage(language1.LanguageId).ToList();
            Assert.That(snippetsWithLanguage.Contains(snippet1), Is.EqualTo(true));
            Assert.That(snippetsWithLanguage.Contains(snippet2), Is.EqualTo(true));
            Assert.That(snippetsWithLanguage.Contains(snippet3), Is.EqualTo(false));
            Assert.That(snippetsWithLanguage.Contains(snippet4), Is.EqualTo(false));

            // Pobieranie wklejek z tagiem
            var snippetsWithTag = snippetRepository_.GetAllSnippetsWithTag(tag1.TagId).ToList();
            Assert.That(snippetsWithTag.Contains(snippet1), Is.EqualTo(true));
            Assert.That(snippetsWithTag.Contains(snippet2), Is.EqualTo(true));
            Assert.That(snippetsWithTag.Contains(snippet3), Is.EqualTo(false));
            Assert.That(snippetsWithTag.Contains(snippet4), Is.EqualTo(false));

            // Pobieranie wklejek z tytułem
            var snippetsWithTitle = snippetRepository_.GetAllSnippetsWithTitle(title).ToList();
            Assert.That(snippetsWithTitle.Contains(snippet1), Is.EqualTo(true));
            Assert.That(snippetsWithTitle.Contains(snippet2), Is.EqualTo(true));
            Assert.That(snippetsWithTitle.Contains(snippet3), Is.EqualTo(true));
            Assert.That(snippetsWithTitle.Contains(snippet4), Is.EqualTo(false));

            // Modyfikacja wklejki
            snippet1.Code = "modified!";
            Assert.That(snippetRepository_.ModifySnippet(snippet1), Is.EqualTo(true));
            Assert.That(snippetRepository_.GetSnippet(snippet1.SnippetId), Is.EqualTo(snippet1));

            // Usunięcie pojedynczej wklejki
            Assert.That(snippetRepository_.DeleteSnippet(snippet1.SnippetId), Is.EqualTo(true));
            Assert.That(snippetRepository_.SnippetExists(snippet1.SnippetId), Is.EqualTo(false));

            // Usunięcie wszystkich wklejek
            Assert.That(snippetRepository_.DeleteAllSnippets(), Is.EqualTo(true));
            Assert.That(snippetRepository_.GetAllSnippets().ToList().Count, Is.EqualTo(0));
        }

        [TearDown]
        public new void TearDown()
        {
            base.TearDown();
        }
    }
}