﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Logic;
using log4net;
using Logic.Ninject;
using Moq;
using Ninject;

namespace Logic.Core
{
    public static class ApplicationLogicFactory
    {
        private static IDatabaseLogic GetDatabaseLogicMock()
        {
            var mock = new Mock<IDatabaseLogic>();

            mock.Setup(_x => _x.CreateDatabase()).Returns(true);
            mock.Setup(_x => _x.DropDatabase()).Returns(true);

            return mock.Object;
        }

        private static IFlagLogic GetFlagLogicMock()
        {
            var mock = new Mock<IFlagLogic>();
            var flag1 = new Flag()
            {
                FlagId = 1,
                FlagName = "ToDo"
            };
            var flag2 = new Flag()
            {
                FlagId = 2,
                FlagName = "Done"
            };
            var flags = new List<string>()
            {
                flag1.FlagName,
                flag2.FlagName
            };

            mock.Setup(_x => _x.GetAllFlags()).Returns(flags);
            mock.Setup(_x => _x.GetFlag(flag1.FlagName)).Returns(flag1);
            mock.Setup(_x => _x.GetFlag(flag2.FlagName)).Returns(flag2);

            return mock.Object;
        }

        private static ILanguageLogic GetLanguageLogicMock()
        {
            var mock = new Mock<ILanguageLogic>();
            var language1 = new Language()
            {
                LanguageId = 1,
                LanguageName = "C#"
            };
            var language2 = new Language()
            {
                LanguageId = 2,
                LanguageName = "Python"
            };
            var languages = new List<string>()
            {
                language1.LanguageName,
                language2.LanguageName
            };

            mock.Setup(_x => _x.GetAllLanguages()).Returns(languages);
            mock.Setup(_x => _x.GetLanguage(language1.LanguageName)).Returns(language1);
            mock.Setup(_x => _x.GetLanguage(language2.LanguageName)).Returns(language2);

            return mock.Object;
        }

        private static ISnippetLogic GetSnippetLogicMock()
        {
            var mock = new Mock<ISnippetLogic>();

            var flag1 = new Flag()
            {
                FlagId = 1,
                FlagName = "ToDo"
            };
            var flag2 = new Flag()
            {
                FlagId = 2,
                FlagName = "Done"
            };
            var language1 = new Language()
            {
                LanguageId = 1,
                LanguageName = "C#"
            };
            var language2 = new Language()
            {
                LanguageId = 2,
                LanguageName = "Python"
            };
            var tag1 = new Tag()
            {
                TagId = 1,
                TagName = "List"
            };
            var tag2 = new Tag()
            {
                TagId = 2,
                TagName = "Adapter"
            };

            var snippet1 = new Snippet(language1, flag1, "Snippet", "snippet1();", "Snippet1 description");
            var snippet2 = new Snippet(language1, flag1, "Snippet", "snippet2();", "Snippet1 description");
            var snippet3 = new Snippet(language1, flag2, "Snippet", "snippet3();", "Snippet1 description");
            var snippet4 = new Snippet(language1, flag2, "Snippet", "snippet4();", "Snippet1 description");
            var snippet5 = new Snippet(language2, flag1, "Snippet", "snippet5();", "Snippet1 description");
            var snippet6 = new Snippet(language2, flag1, "Snippet", "snippet6();", "Snippet1 description");
            var snippet7 = new Snippet(language2, flag2, "KaBoom", "snippet7();", "Snippet1 description");
            var snippet8 = new Snippet(language2, flag2, "KaBoom", "snippet8();", "Snippet1 description");

            var snippets = new List<Snippet>()
            {
                snippet1,
                snippet2,
                snippet3,
                snippet4,
                snippet5,
                snippet6,
                snippet7,
                snippet8
            };

            mock.Setup(_x => _x.GetAllSnippets()).Returns(snippets);

            mock.Setup(_x => _x.GetAllSnippetsWithFlag(flag1.FlagName))
                .Returns(snippets.Where(_s => _s.Flag.FlagName == flag1.FlagName));
            mock.Setup(_x => _x.GetAllSnippetsWithFlag(flag2.FlagName))
                .Returns(snippets.Where(_s => _s.Flag.FlagName == flag2.FlagName));

            mock.Setup(_x => _x.GetAllSnippetsWithLanguage(language1.LanguageName))
                .Returns(snippets.Where(_s => _s.Language.LanguageName == language1.LanguageName));
            mock.Setup(_x => _x.GetAllSnippetsWithLanguage(language2.LanguageName))
                .Returns(snippets.Where(_s => _s.Language.LanguageName == language2.LanguageName));

            mock.Setup(_x => _x.GetAllSnippetsWithTag(tag1.TagName))
                .Returns(new List<Snippet>() { snippet1, snippet3, snippet5, snippet7 });
            mock.Setup(_x => _x.GetAllSnippetsWithTag(tag1.TagName))
                .Returns(new List<Snippet>() { snippet1, snippet3, snippet5, snippet7 });

            mock.Setup(_x => _x.GetAllSnippetsWithTitle("Snippet"))
                .Returns(new List<Snippet>() { snippet1, snippet2, snippet3, snippet4, snippet5, snippet6 });
            mock.Setup(_x => _x.GetAllSnippetsWithTitle("KaBoom"))
                .Returns(new List<Snippet>() { snippet7, snippet8 });

            mock.Setup(_x => _x.GetAllMatchingSnippets(It.IsAny<Func<Snippet, bool>>())).Throws(new System.NotImplementedException());

            return mock.Object;
        }

        private static ITagAssignmentLogic GetTagAssignmentLogicMock()
        {
            var mock = new Mock<ITagAssignmentLogic>();
            // Tutaj ustawiamy mocka
            return mock.Object;
        }

        private static ITagLogic GetTagLogicMock()
        {
            var mock = new Mock<ITagLogic>();
            var tag1 = new Tag()
            {
                TagId = 1,
                TagName = "List"
            };
            var tag2 = new Tag()
            {
                TagId = 2,
                TagName = "Adapter"
            };
            var tags = new List<string>()
            {
                tag1.TagName,
                tag2.TagName
            };

            mock.Setup(_x => _x.GetAllTags()).Returns(tags);
            mock.Setup(_x => _x.GetTag(tag1.TagName)).Returns(tag1);
            mock.Setup(_x => _x.GetTag(tag2.TagName)).Returns(tag2);

            return mock.Object;
        }

        public enum Mode
        {
            Real,
            Fake
        }

        public static IApplicationLogic GetApplicationLogic(Mode _mode, string _databaseFileName = null, ILog _logger = null)
        {
            if (_mode != Mode.Real)
            {
                // Tworzenie fake'owego ApplicationLogic
                return new ApplicationLogic(
                    GetDatabaseLogicMock(),
                    GetFlagLogicMock(),
                    GetLanguageLogicMock(),
                    GetSnippetLogicMock(),
                    GetTagAssignmentLogicMock(),
                    GetTagLogicMock());
            }

            // Tworzenie prawdziwego ApplicationLogic
            if (string.IsNullOrEmpty(_databaseFileName)
                || _logger == null) return null;

            return new CodeBookKernel(_databaseFileName, _logger).Get<IApplicationLogic>();
        }
    }
}