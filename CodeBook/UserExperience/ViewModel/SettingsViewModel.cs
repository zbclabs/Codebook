﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using UserExperience.Requests;
using Microsoft.Win32;
using System.IO;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Reflection;
using Logic.Core;
using log4net;

namespace UserExperience.ViewModel
{
    public class AccentColorMenuData
    {
        public Brush BorderColorBrush { get; set; }

        public Brush ColorBrush { get; set; }

        public string Name { get; set; }

        public virtual void DoChangeTheme(object _sender = null)
        {
            var theme = ThemeManager.DetectAppStyle(Application.Current);
            var accent = ThemeManager.GetAccent(this.Name);
            ThemeManager.ChangeAppStyle(Application.Current, accent, theme.Item1);
        }
    }

    public class AppThemeMenuData : AccentColorMenuData
    {
        public override void DoChangeTheme(object _sender = null)
        {
            var theme = ThemeManager.DetectAppStyle(Application.Current);
            var appTheme = ThemeManager.GetAppTheme(this.Name);
            ThemeManager.ChangeAppStyle(Application.Current, theme.Item2, appTheme);
        }
    }

    public class SettingsViewModel : BindableBase
    {
        private int selectedAccentIndex_;
        private string selectedDatabasePath_;
        private string selectedStartupCategory_;
        private int selectedThemeIndex_;

        public List<AccentColorMenuData> AccentColors { get; set; }
        public ICommand AddNewDatabaseCommand { get; }
        public List<AppThemeMenuData> AppThemes { get; set; }
        public ICommand DeleteDatabaseCommand { get; }
        public ICommand OpenWebsiteCommand { get; }

        public string ProgramVersion
        {
            get
            {
                var numbers = Assembly.GetEntryAssembly().GetName().Version.ToString().Split('.');
                return $"{numbers[0]}.{numbers[1]}.{numbers[2]}";
            }
        }
        public ICommand SaveSettingsCommand { get; }

        public int SelectedAccentIndex
        {
            get => selectedAccentIndex_;
            set
            {
                SetProperty(ref selectedAccentIndex_, value);
                OnPropertyChanged("SelectedAccentIndex");
            }
        }

        public string SelectedDatabasePath
        {
            get => selectedDatabasePath_;
            set
            {
                SetProperty(ref selectedDatabasePath_, value);
                OnPropertyChanged("SelectedDatabasePath");
            }
        }

        public string SelectedStartupCategory
        {
            get => selectedStartupCategory_;
            set
            {
                SetProperty(ref selectedStartupCategory_, value);
                OnPropertyChanged("SelectedStartupCategory");
            }
        }

        public int SelectedThemeIndex
        {
            get => selectedThemeIndex_;
            set
            {
                SetProperty(ref selectedThemeIndex_, value);
                OnPropertyChanged("SelectedThemeIndex");
            }
        }

        public ICommand SettingsClosedCommand { get; }
        public ICommand UploadDatabaseCommand { get; }

        public SettingsViewModel()
        {
            AddNewDatabaseCommand = new DelegateCommand(AddNewDatabaseHandler);
            DeleteDatabaseCommand = new DelegateCommand(DeleteDatabaseHandler);
            OpenWebsiteCommand = new DelegateCommand(OpenWebsiteHandler);
            SaveSettingsCommand = new DelegateCommand(SaveSettingsHandler);
            SettingsClosedCommand = new DelegateCommand(SettingsClosedHandler);
            UploadDatabaseCommand = new DelegateCommand(UploadDatabaseHandler);

            AccentColors = ThemeManager.Accents
                            .Select(a => new AccentColorMenuData() { Name = a.Name, ColorBrush = a.Resources["AccentColorBrush"] as Brush })
                            .ToList();

            AppThemes = ThemeManager.AppThemes
                            .Select(a => new AppThemeMenuData() { Name = a.Name, BorderColorBrush = a.Resources["BlackColorBrush"] as Brush, ColorBrush = a.Resources["WhiteColorBrush"] as Brush })
                            .ToList();

            SelectedAccentIndex = AccentColors.FindIndex(new Predicate<AccentColorMenuData>(_x => _x.Name == Properties.Settings.Default["Accent"].ToString()));
            SelectedDatabasePath = Properties.Settings.Default["DatabasePath"].ToString();
            SelectedStartupCategory = Properties.Settings.Default["StartupCategory"].ToString();
            SelectedThemeIndex = AppThemes.FindIndex(new Predicate<AppThemeMenuData>(_x => _x.Name == Properties.Settings.Default["Theme"].ToString()));
        }

        public async void AddNewDatabaseHandler()
        {
            var saveFileDialog = new SaveFileDialog()
            {
                Filter = "CodeBook files (*.qbook)|*.qbook",
                AddExtension = true,
                DefaultExt = "qbook"
            };
            saveFileDialog.ShowDialog();

            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
                SelectedDatabasePath = Path.ChangeExtension(saveFileDialog.FileName, "qbook");

            log4net.Config.XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("RollingFileAppender");

            if (!ApplicationLogicFactory.GetApplicationLogic(ApplicationLogicFactory.Mode.Real,
                SelectedDatabasePath, logger).DatabaseLogic.CreateDatabase())
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                await metroWindow.ShowMessageAsync("Database creation error",
                        "Cannot create database!", MessageDialogStyle.Affirmative);
            }
        }

        public async void DeleteDatabaseHandler()
        {
            var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
            var result = await metroWindow.ShowMessageAsync("Delete database",
                "Do you really want to delete database?", MessageDialogStyle.AffirmativeAndNegative);

            if (result == MessageDialogResult.Affirmative)
            {
                try
                {
                    File.Delete(SelectedDatabasePath);
                    SelectedDatabasePath = string.Empty;
                }
                catch (Exception)
                {
                    await metroWindow.ShowMessageAsync("Delete database",
                        "Cannot delete database!", MessageDialogStyle.Affirmative);
                }
            }
        }

        public void OpenWebsiteHandler()
        {
            Process.Start("https://zbclabs.ml/CodeBook");
        }

        public async void SaveSettingsHandler()
        {
            var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
            var result = await metroWindow.ShowMessageAsync("Restart",
                "Settings change require restarting application. Do you want to continue?", MessageDialogStyle.AffirmativeAndNegative);

            if (result == MessageDialogResult.Affirmative)
            {
                Properties.Settings.Default["Accent"] = AccentColors[SelectedAccentIndex].Name;
                Properties.Settings.Default["DatabasePath"] = SelectedDatabasePath;
                Properties.Settings.Default["StartupCategory"] = SelectedStartupCategory;
                Properties.Settings.Default["Theme"] = AppThemes[SelectedThemeIndex].Name;
                Properties.Settings.Default.Save();

                Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }
        }

        public void SettingsClosedHandler()
        {
            Messenger.Default.Send(new SettingsClosedRequest());
        }

        public void UploadDatabaseHandler()
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = "CodeBook files (*.qbook)|*.qbook",
                AddExtension = true,
                DefaultExt = "qbook"
            };
            openFileDialog.ShowDialog();

            if (!string.IsNullOrEmpty(openFileDialog.FileName))
                SelectedDatabasePath = Path.ChangeExtension(openFileDialog.FileName, "qbook");
        }
    }
}