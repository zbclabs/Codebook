﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class SnippetLogic : ISnippetLogic
    {
        private readonly IFlagRepository flagRepository_;
        private readonly ILanguageRepository languageRepository_;
        private readonly ILog logger_;
        private readonly ISnippetRepository snippetRepository_;
        private readonly ITagRepository tagRepository_;

        public SnippetLogic(
            ILog _logger,
            ILanguageRepository _languageRepository,
            IFlagRepository _flagRepository,
            ISnippetRepository _snippetRepository,
            ITagRepository _tagRepository)
        {
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
            languageRepository_ = _languageRepository ?? throw new ArgumentException("LanguageRepository cannot be null!");
            flagRepository_ = _flagRepository ?? throw new ArgumentException("FlagRepository cannot be null!");
            snippetRepository_ = _snippetRepository ?? throw new ArgumentException("SnippetRepository cannot be null!");
            tagRepository_ = _tagRepository ?? throw new ArgumentException("TagRepository cannot be null!");
        }

        public bool AddSnippet(Snippet _snippet)
        {
            if (_snippet == null)
            {
                logger_.Warn("Snippet is null, returning false...");
                return false;
            }
            if (!languageRepository_.LanguageExists(_snippet.Language.LanguageName))
            {
                logger_.Info($"{_snippet.Language.LanguageName} language doesn't exist, creating one...");
                languageRepository_.AddLanguage(_snippet.Language.LanguageName);
                _snippet.Language = languageRepository_.GetLanguage(_snippet.Language.LanguageName);
            }
            if (!flagRepository_.FlagExists(_snippet.Flag.FlagName))
            {
                logger_.Info($"{_snippet.Flag.FlagName} flag doesn't exist, creating one...");
                flagRepository_.AddFlag(_snippet.Flag.FlagName);
                _snippet.Flag = flagRepository_.GetFlag(_snippet.Flag.FlagName);
            }

            return snippetRepository_.AddSnippet(_snippet);
        }

        public bool DeleteAllSnippets()
        {
            return snippetRepository_.DeleteAllSnippets();
        }

        public bool DeleteSnippet(Guid _snippetId)
        {
            if (snippetRepository_.SnippetExists(_snippetId)) return snippetRepository_.DeleteSnippet(_snippetId);
            logger_.Warn("Snippet doesn't exist, returning false...");
            return false;
        }

        public IEnumerable<Snippet> GetAllMatchingSnippets(Func<Snippet, bool> _predicate)
        {
            if (_predicate == null)
            {
                logger_.Warn("Predicate is null, returning null...");
                return null;
            }

            var snippets = snippetRepository_.GetAllSnippets();
            return snippets.Where(_predicate);
        }

        public IEnumerable<Snippet> GetAllSnippets()
        {
            return snippetRepository_.GetAllSnippets();
        }

        public IEnumerable<Snippet> GetAllSnippetsWithFlag(string _flagName)
        {
            if (string.IsNullOrEmpty(_flagName))
            {
                logger_.Warn("FlagName is null or empty, returning null...");
                return null;
            }
            var flag = flagRepository_.GetFlag(_flagName);
            if (flag != null) return snippetRepository_.GetAllSnippetsWithFlag(flag.FlagId);
            logger_.Warn("Flag doesn't exist, returning null");
            return null;
        }

        public IEnumerable<Snippet> GetAllSnippetsWithLanguage(string _languageName)
        {
            if (string.IsNullOrEmpty(_languageName))
            {
                logger_.Warn("LanguageName is null or empty, returning null...");
                return null;
            }
            var language = languageRepository_.GetLanguage(_languageName);
            if (language != null) return snippetRepository_.GetAllSnippetsWithLanguage(language.LanguageId);
            logger_.Warn("Language doesn't exist, returning null");
            return null;
        }

        public IEnumerable<Snippet> GetAllSnippetsWithTag(string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning null...");
                return null;
            }
            var tag = tagRepository_.GetTag(_tagName);
            if (tag != null) return snippetRepository_.GetAllSnippetsWithTag(tag.TagId);
            logger_.Error("Tag doesn't exist, returning null");
            return null;
        }

        public IEnumerable<Snippet> GetAllSnippetsWithTitle(string _title)
        {
            if (!string.IsNullOrEmpty(_title)) return snippetRepository_.GetAllSnippetsWithTitle(_title);
            logger_.Warn("Title is null or empty, returning null...");
            return null;
        }

        public bool ModifySnippet(Snippet _snippet)
        {
            if (_snippet == null)
            {
                logger_.Warn("Snippet is null, returning false...");
                return false;
            }
            if (!snippetRepository_.SnippetExists(_snippet.SnippetId))
            {
                logger_.Warn("Snippet doesn't exist, returning false...");
                return false;
            }
            if (!languageRepository_.LanguageExists(_snippet.Language.LanguageName))
            {
                logger_.Info($"{_snippet.Language.LanguageName} language doesn't exist, creating one...");
                languageRepository_.AddLanguage(_snippet.Language.LanguageName);
                _snippet.Language = languageRepository_.GetLanguage(_snippet.Language.LanguageName);
            }
            if (!flagRepository_.FlagExists(_snippet.Flag.FlagName))
            {
                logger_.Info($"{_snippet.Flag.FlagName} flag doesn't exist, creating one...");
                flagRepository_.AddFlag(_snippet.Flag.FlagName);
                _snippet.Flag = flagRepository_.GetFlag(_snippet.Flag.FlagName);
            }

            _snippet.LastEditTime = DateTime.Now;
            return snippetRepository_.ModifySnippet(_snippet);
        }

        public bool SnippetExists(Guid _snippetId)
        {
            return snippetRepository_.SnippetExists(_snippetId);
        }
    }
}