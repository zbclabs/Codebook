﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Repository;
using log4net;
using Logic.Core;
using Moq;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class LanguageLogicTests
    {
        private LanguageLogic languageLogic_;
        private Mock<ILanguageRepository> languageRepositoryMock_;
        private Mock<ILog> loggerMock_;

        [Test, Category("Unit")]
        public void AddLanguage_CorrectLanguageName()
        {
            var languageName = "C#";
            var language = new Language(languageName);
            languageRepositoryMock_.Setup(_x => _x.AddLanguage(languageName)).Returns(true);
            languageRepositoryMock_.Setup(_x => _x.GetLanguage(languageName)).Returns(language);
            Assert.AreEqual(language, languageLogic_.AddLanguage(languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            languageRepositoryMock_.Verify(_x => _x.AddLanguage(languageName), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void AddLanguage_IncorrectLanguageName(string _languageName)
        {
            Assert.AreEqual(null, languageLogic_.AddLanguage(_languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.AddLanguage(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteAllLanguages()
        {
            languageRepositoryMock_.Setup(_x => _x.DeleteAllLanguages()).Returns(true);
            Assert.AreEqual(true, languageLogic_.DeleteAllLanguages());
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            languageRepositoryMock_.Verify(_x => _x.DeleteAllLanguages(), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void DeleteLanguage_IncorrectLanguageName(string _languageName)
        {
            Assert.AreEqual(false, languageLogic_.DeleteLanguage(_languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(x => x.DeleteLanguage(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteLanguage_LanguageDoesntExist()
        {
            var language = "C#";
            languageRepositoryMock_.Setup(_x => _x.LanguageExists(language)).Returns(false);
            Assert.AreEqual(false, languageLogic_.DeleteLanguage(language));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.DeleteLanguage(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteLanguage_LanguageExists()
        {
            var language = "C#";
            languageRepositoryMock_.Setup(_x => _x.LanguageExists(language)).Returns(true);
            languageRepositoryMock_.Setup(_x => _x.DeleteLanguage(language)).Returns(true);
            Assert.AreEqual(true, languageLogic_.DeleteLanguage(language));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            languageRepositoryMock_.Verify(_x => _x.DeleteLanguage(It.IsAny<string>()), Times.Once);
        }

        [Test, Category("Unit")]
        public void GetAllLanguages()
        {
            var languages = new List<string> { "C#", "VB.NET", "Python" };
            languageRepositoryMock_.Setup(_x => _x.GetAllLanguages()).Returns(languages);
            Assert.AreEqual(languages, languageLogic_.GetAllLanguages());
        }

        [Test, Category("Unit")]
        public void GetLanguage_CorrectLanguageName()
        {
            var language = "C#";
            languageLogic_.GetLanguage(language);
            languageRepositoryMock_.Verify(_x => _x.GetLanguage(language), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetLanguage_IncorrectLanguageName(string _languageName)
        {
            Assert.AreEqual(null, languageLogic_.GetLanguage(_languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.GetLanguage(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void LanguageExists_CorrectLanguageName()
        {
            var language = "C#";
            languageLogic_.LanguageExists(language);
            languageRepositoryMock_.Verify(_x => _x.LanguageExists(language), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void LanguageExists_IncorrectLanguageName(string _languageName)
        {
            Assert.AreEqual(false, languageLogic_.LanguageExists(_languageName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.LanguageExists(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        public void LanguageLogicConstructor_IncorrectArguments(bool _isLoggerNull, bool _isLanguageRepositoryNull)
        {
            var logger = _isLoggerNull ? null : loggerMock_.Object;
            var languageRepository = _isLanguageRepositoryNull ? null : languageRepositoryMock_.Object;

            try
            {
                var languageLogic = new LanguageLogic(logger, languageRepository);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
        }

        [Test, Category("Unit")]
        public void ModifyLanguage_CorrectLanguage()
        {
            var language = new Language("C#");
            languageLogic_.ModifyLanguage(language);
            languageRepositoryMock_.Verify(_x => _x.ModifyLanguage(language), Times.Once);
        }

        [Test, Category("Unit")]
        public void ModifyLanguage_NullLanguage()
        {
            Assert.AreEqual(false, languageLogic_.ModifyLanguage(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            languageRepositoryMock_.Verify(_x => _x.ModifyLanguage(It.IsAny<Language>()), Times.Never);
        }

        [SetUp]
        public void SetUp()
        {
            languageRepositoryMock_ = new Mock<ILanguageRepository>();
            loggerMock_ = new Mock<ILog>();
            loggerMock_.Setup(_x => _x.Warn(It.IsAny<string>()));
            languageLogic_ = new LanguageLogic(loggerMock_.Object, languageRepositoryMock_.Object);
        }
    }
}