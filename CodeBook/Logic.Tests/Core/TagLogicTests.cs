﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Repository;
using log4net;
using Logic.Core;
using Moq;
using NUnit.Framework;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class TagLogicTests
    {
        private Mock<ILog> loggerMock_;
        private TagLogic tagLogic_;
        private Mock<ITagRepository> tagRepositoryMock_;

        [Test, Category("Unit")]
        public void AddTag_CorrectTagName()
        {
            var tagName = "List";
            var tag = new Tag(tagName);
            tagRepositoryMock_.Setup(_x => _x.AddTag(tagName)).Returns(true);
            tagRepositoryMock_.Setup(_x => _x.GetTag(tagName)).Returns(tag);
            Assert.AreEqual(tag, tagLogic_.AddTag(tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            tagRepositoryMock_.Verify(_x => _x.AddTag(tagName), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void AddTag_IncorrectTagName(string _tagName)
        {
            Assert.AreEqual(null, tagLogic_.AddTag(_tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.AddTag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteAllTags()
        {
            tagRepositoryMock_.Setup(_x => _x.DeleteAllTags()).Returns(true);
            Assert.AreEqual(true, tagLogic_.DeleteAllTags());
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            tagRepositoryMock_.Verify(_x => _x.DeleteAllTags(), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void DeleteTag_IncorrectTagName(string _tagName)
        {
            Assert.AreEqual(false, tagLogic_.DeleteTag(_tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(x => x.DeleteTag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteTag_TagDoesntExist()
        {
            var tag = "List";
            tagRepositoryMock_.Setup(_x => _x.TagExists(tag)).Returns(false);
            Assert.AreEqual(false, tagLogic_.DeleteTag(tag));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.DeleteTag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteTag_TagExists()
        {
            var tag = "List";
            tagRepositoryMock_.Setup(_x => _x.TagExists(tag)).Returns(true);
            tagRepositoryMock_.Setup(_x => _x.DeleteTag(tag)).Returns(true);
            Assert.AreEqual(true, tagLogic_.DeleteTag(tag));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            tagRepositoryMock_.Verify(_x => _x.DeleteTag(It.IsAny<string>()), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        public void FlagLogicConstructor_IncorrectArguments(bool _isLoggerNull, bool _isTagRepositoryNull)
        {
            var logger = _isLoggerNull ? null : loggerMock_.Object;
            var tagRepository = _isTagRepositoryNull ? null : tagRepositoryMock_.Object;

            try
            {
                var tagLogic = new TagLogic(logger, tagRepository);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
        }

        [Test, Category("Unit")]
        public void GetAllTags()
        {
            var tags = new List<string> { "List", "Adapter", "Static" };
            tagRepositoryMock_.Setup(_x => _x.GetAllTags()).Returns(tags);
            Assert.AreEqual(tags, tagLogic_.GetAllTags());
        }

        [Test, Category("Unit")]
        public void GetTag_CorrectTagName()
        {
            var tag = "List";
            tagLogic_.GetTag(tag);
            tagRepositoryMock_.Verify(_x => _x.GetTag(tag), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetTag_IncorrectTagName(string _tagName)
        {
            Assert.AreEqual(null, tagLogic_.GetTag(_tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.GetTag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void ModifyTag_CorrectTag()
        {
            var tag = new Tag("List");
            tagLogic_.ModifyTag(tag);
            tagRepositoryMock_.Verify(_x => _x.ModifyTag(tag), Times.Once);
        }

        [Test, Category("Unit")]
        public void ModifyTag_NullTag()
        {
            Assert.AreEqual(false, tagLogic_.ModifyTag(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.ModifyTag(It.IsAny<Tag>()), Times.Never);
        }

        [SetUp]
        public void SetUp()
        {
            tagRepositoryMock_ = new Mock<ITagRepository>();
            loggerMock_ = new Mock<ILog>();
            loggerMock_.Setup(_x => _x.Warn(It.IsAny<string>()));
            tagLogic_ = new TagLogic(loggerMock_.Object, tagRepositoryMock_.Object);
        }

        [Test, Category("Unit")]
        public void TagExists_CorrectTagName()
        {
            var tag = "List";
            tagLogic_.TagExists(tag);
            tagRepositoryMock_.Verify(_x => _x.TagExists(tag), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void TagExists_IncorrectTagName(string _tagName)
        {
            Assert.AreEqual(false, tagLogic_.TagExists(_tagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            tagRepositoryMock_.Verify(_x => _x.TagExists(It.IsAny<string>()), Times.Never);
        }
    }
}