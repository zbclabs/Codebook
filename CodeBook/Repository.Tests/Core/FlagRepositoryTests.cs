﻿using System.Linq;
using NUnit.Framework;
using Repository.Core;

namespace Repository.Tests.Core
{
    [TestFixture]
    public class FlagRepositoryTests : RepositoryTestBase
    {
        private FlagRepository flagRepository_;

        [Test, Category("Integration")]
        public void FlagRepositoryTest()
        {
            // Flagi jeszcze nie istnieją - te metody powinny zwrócić false lub null
            Assert.That(flagRepository_.FlagExists("Flag"), Is.EqualTo(false));
            Assert.That(flagRepository_.GetAllFlags().ToList().Count, Is.EqualTo(0));
            Assert.That(flagRepository_.GetFlag("Flag"), Is.EqualTo(null));

            // Utworzenie flag - te metody powinny przejść
            var flagName1 = "Done";
            var flagName2 = "InProgress";
            Assert.That(flagRepository_.AddFlag(flagName1), Is.EqualTo(true));
            Assert.That(flagRepository_.AddFlag(flagName2), Is.EqualTo(true));

            // Utworzenie duplikatu - ta metoda powinna nawalić
            Assert.That(flagRepository_.AddFlag(flagName1), Is.EqualTo(false));

            // Odczyt wszystkich flag
            var flag1 = flagRepository_.GetFlag(flagName1);
            Assert.AreNotEqual(null, flag1);
            var flag2 = flagRepository_.GetFlag(flagName2);
            Assert.AreNotEqual(null, flag2);
            var allFlags = flagRepository_.GetAllFlags().ToList();
            Assert.That(allFlags.Contains(flagName1), Is.EqualTo(true));
            Assert.That(allFlags.Contains(flagName2), Is.EqualTo(true));

            // Sprawdzenie, czy flagi istnieją
            Assert.That(flagRepository_.FlagExists(flagName1), Is.EqualTo(true));
            Assert.That(flagRepository_.FlagExists(flagName2), Is.EqualTo(true));
            Assert.That(flagRepository_.FlagExists("Nope"), Is.EqualTo(false));

            // Modyfikacja flagi
            flag1.FlagName = "ToDo";
            Assert.That(flagRepository_.ModifyFlag(flag1), Is.EqualTo(true));
            Assert.That(flagRepository_.GetFlag(flag1.FlagName), Is.EqualTo(flag1));

            // Usunięcie pojedynczej flagi
            Assert.That(flagRepository_.DeleteFlag(flag1.FlagName), Is.EqualTo(true));
            Assert.That(flagRepository_.FlagExists(flag1.FlagName), Is.EqualTo(false));

            // Usunięcie wszystkich flag
            Assert.That(flagRepository_.DeleteAllFlags(), Is.EqualTo(true));
            Assert.That(flagRepository_.GetAllFlags().ToList().Count, Is.EqualTo(0));
        }

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            flagRepository_ = new FlagRepository(ConnectionString, Logger);
        }

        [TearDown]
        public new void TearDown()
        {
            base.TearDown();
        }
    }
}