﻿using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Repository
{
    public interface ILanguageRepository
    {
        bool AddLanguage(string _languageName);

        bool DeleteAllLanguages();

        bool DeleteLanguage(string _languageName);

        IEnumerable<string> GetAllLanguages();

        Language GetLanguage(string _languageName);

        bool LanguageExists(string _languageName);

        bool ModifyLanguage(Language _language);
    }
}