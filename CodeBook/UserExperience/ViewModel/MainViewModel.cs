﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using Api.Dto;
using Api.Interfaces.Logic;
using GalaSoft.MvvmLight.Messaging;
using ICSharpCode.AvalonEdit.Document;
using log4net;
using Logic.Core;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using UserExperience.Requests;
using UserExperience.Views;
using MahApps.Metro;
using System.Windows.Media;

namespace UserExperience.ViewModel
{
    public class MainViewModel : BindableBase
    {
        private string addTagTextBoxContent_;
        private ObservableCollection<string> allFlags_;
        private ObservableCollection<string> allLanguages_;
        private string editSaveButtonContent_;
        private int hamburgerItemSelectedIndex_;
        private bool isFlagToggleChecked_;
        private bool isFlyoutOpen_;
        private bool isLanguageToggleChecked_;
        private bool isSettingsWindowOpened_;
        private bool isSnippetReadOnly_;
        private bool isTagToggleChecked_;
        private string searchBoxContent_;
        private Snippet selectedSnippet_;
        private TextDocument snippetCode_;
        private DateTime snippetCreationTime_;
        private string snippetDescription_;
        private string snippetFlag_;
        private string snippetId_;
        private string snippetLanguage_;
        private DateTime snippetLastEditTime_;
        private List<Snippet> snippetList_;
        private ObservableCollection<string> snippetTags_;
        private string snippetTitle_;
        private ObservableCollection<string> tabControlContent_;
        private string tabControlCurrentSelection_;
        private string windowTitle_;

        private void InitializeCommands()
        {
            AddTagCommand = new DelegateCommand(AddTagHandler);
            ApplicationClosingCommand = new DelegateCommand(ApplicationClosingHandler);
            CancelClickCommand = new DelegateCommand(CancelClickHandler);
            CopySnippetCodeCommand = new DelegateCommand(CopySnippetCodeHandler);
            DeleteSnippetCommand = new DelegateCommand(DeleteSnippetHandler);
            DeleteTagCommand = new DelegateCommand<string>(DeleteTagHandler);
            EditSaveClickCommand = new DelegateCommand(EditSaveClickHandler);
            HamburgerMenuItemClickedCommand = new DelegateCommand(HamburgerMenuItemClickedHandler);
            HamburgerMenuOptionsItemClickedCommand = new DelegateCommand(HamburgerMenuOptionsItemClickedHandler);
            OpenFlyoutCommand = new DelegateCommand(OpenFlyoutHandler);
            SearchCommand = new DelegateCommand(SearchHandler);
            SettingsClickCommand = new DelegateCommand(SettingsClickHandler);
            TabItemSelectedCommand = new DelegateCommand<SelectionChangedEventArgs>(TabItemSelectedHandler);
        }

        private void LoadSelectedSnippet()
        {
            if (SnippetList != null && SnippetList.Count > 0)
            {
                AllLanguages = new ObservableCollection<string>(ApplicationLogic.LanguageLogic.GetAllLanguages());
                AllFlags = new ObservableCollection<string>(ApplicationLogic.FlagLogic.GetAllFlags());

                SelectedSnippet = (Snippet)SnippetList[HamburgerMenuSelectedIndex].Clone();

                SnippetCode.Text = SelectedSnippet.Code;
                SnippetCreationTime = SelectedSnippet.CreationTime;
                SnippetDescription = SelectedSnippet.SnippetDescription;
                SnippetFlag = SelectedSnippet.Flag.FlagName;
                SnippetId = SelectedSnippet.SnippetId.ToString();
                SnippetLanguage = SelectedSnippet.Language.LanguageName;
                SnippetLastEditTime = SelectedSnippet.LastEditTime;
                SnippetTags = new ObservableCollection<string>(ApplicationLogic.TagAssignmentLogic.GetSnippetTags(SelectedSnippet.SnippetId));
                SnippetTitle = SelectedSnippet.Title;
                WindowTitle = $"CodeBook \\ {SelectedSnippet.Title}";
            }
            else
            {
                SelectedSnippet = null;
                SnippetCode.Text = string.Empty;
                SnippetCreationTime = DateTime.Now;
                SnippetDescription = string.Empty;
                SnippetFlag = string.Empty;
                SnippetId = string.Empty;
                SnippetLanguage = string.Empty;
                SnippetLastEditTime = SnippetCreationTime;
                SnippetTags = new ObservableCollection<string>();
                SnippetTitle = string.Empty;
                WindowTitle = "CodeBook";
            }
        }

        private void ReloadAll()
        {
            if (IsFlagToggleChecked)
                IsFlagToggleChecked = true;
            else if (IsLanguageToggleChecked)
                IsLanguageToggleChecked = true;
            else if (IsTagToggleChecked)
                IsTagToggleChecked = true;
            TabItemSelectedHandler();
        }

        private void SetApplicationLogic()
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("RollingFileAppender");

            IApplicationLogic applicationLogic = null;

            try
            {
                var path = Environment.ExpandEnvironmentVariables(
                    Properties.Settings.Default["DatabasePath"].ToString());

                applicationLogic =
                    ApplicationLogicFactory.GetApplicationLogic(ApplicationLogicFactory.Mode.Real, path, logger);

                if (!File.Exists(path))
                {
                    var directory = Path.GetDirectoryName(path);

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    if (!applicationLogic.DatabaseLogic.CreateDatabase())
                    {
                        throw new Exception("Cannot create database file!");
                    }
                }
            }
            catch (Exception _exception)
            {
                logger.Error("Cannot load database, loading mock...", _exception);
                applicationLogic = ApplicationLogicFactory.GetApplicationLogic(ApplicationLogicFactory.Mode.Fake);

                MessageBox.Show("Cannot load database. Please set correct path in Settings.", "Critical error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                ApplicationLogic = applicationLogic;
            }
        }

        private void TurnOffToggles()
        {
            SetProperty(ref isFlagToggleChecked_, false);
            SetProperty(ref isLanguageToggleChecked_, false);
            SetProperty(ref isTagToggleChecked_, false);
            OnPropertyChanged("IsFlagToggleChecked");
            OnPropertyChanged("IsLanguageToggleChecked");
            OnPropertyChanged("IsTagToggleChecked");
        }

        public ICommand AddTagCommand { get; private set; }

        public string AddTagTextBoxContent
        {
            get => addTagTextBoxContent_;
            set
            {
                SetProperty(ref addTagTextBoxContent_, value);
                OnPropertyChanged("AddTagTextBoxContent");
            }
        }

        public ObservableCollection<string> AllFlags
        {
            get => allFlags_;
            set
            {
                SetProperty(ref allFlags_, value);
                OnPropertyChanged("AllFlags");
            }
        }

        public ObservableCollection<string> AllLanguages
        {
            get => allLanguages_;
            set
            {
                SetProperty(ref allLanguages_, value);
                OnPropertyChanged("AllLanguages");
            }
        }

        public ICommand ApplicationClosingCommand { get; private set; }
        public IApplicationLogic ApplicationLogic { get; private set; }
        public ICommand CancelClickCommand { get; private set; }
        public ICommand CopySnippetCodeCommand { get; private set; }
        public ICommand DeleteSnippetCommand { get; private set; }
        public ICommand DeleteTagCommand { get; private set; }

        public string EditSaveButtonContent
        {
            get => editSaveButtonContent_;
            set
            {
                SetProperty(ref editSaveButtonContent_, value);
                OnPropertyChanged("EditSaveButtonContent");
            }
        }

        public ICommand EditSaveClickCommand { get; private set; }
        public ICommand HamburgerMenuItemClickedCommand { get; private set; }
        public ICommand HamburgerMenuOptionsItemClickedCommand { get; private set; }

        public int HamburgerMenuSelectedIndex
        {
            get => hamburgerItemSelectedIndex_;
            set
            {
                SetProperty(ref hamburgerItemSelectedIndex_, value);
                OnPropertyChanged("HamburgerMenuSelectedIndex");
            }
        }

        public bool IsFlagToggleChecked
        {
            get => isFlagToggleChecked_;
            set
            {
                if (!value) return;
                TurnOffToggles();
                SetProperty(ref isFlagToggleChecked_, true);
                var newContent = new ObservableCollection<string>(ApplicationLogic.FlagLogic.GetAllFlags());
                if (newContent.Count == 0) newContent.Add("Empty");
                TabControlCurrentSelection = newContent?[0];
                TabControlContent = newContent;
            }
        }

        public bool IsFlyoutOpen
        {
            get => isFlyoutOpen_;
            set
            {
                SetProperty(ref isFlyoutOpen_, value);
                OnPropertyChanged("IsFlyoutOpen");
            }
        }

        public bool IsLanguageToggleChecked
        {
            get => isLanguageToggleChecked_;
            set
            {
                if (!value) return;
                TurnOffToggles();
                SetProperty(ref isLanguageToggleChecked_, true);
                OnPropertyChanged("IsLanguageToggleChecked");
                var newContent = new ObservableCollection<string>(ApplicationLogic.LanguageLogic.GetAllLanguages());
                if (newContent.Count == 0) newContent.Add("Empty");
                TabControlCurrentSelection = newContent?[0];
                TabControlContent = newContent;
            }
        }

        public bool IsNewSnippet { get; set; }

        public bool IsSnippetReadOnly
        {
            get => isSnippetReadOnly_;
            set
            {
                SetProperty(ref isSnippetReadOnly_, value);
                EditSaveButtonContent = value ? "Edit" : "Save";
            }
        }

        public bool IsTagToggleChecked
        {
            get => isTagToggleChecked_;
            set
            {
                if (!value) return;
                TurnOffToggles();
                SetProperty(ref isTagToggleChecked_, true);
                OnPropertyChanged("IsTagToggleChecked");
                var newContent = new ObservableCollection<string>(ApplicationLogic.TagLogic.GetAllTags());
                if (newContent.Count == 0) newContent.Add("Empty");
                TabControlCurrentSelection = newContent[0];
                TabControlContent = newContent;
            }
        }

        public ICommand OpenFlyoutCommand { get; private set; }

        public string SearchBoxContent
        {
            get => searchBoxContent_;
            set => SetProperty(ref searchBoxContent_, value);
        }

        public ICommand SearchCommand { get; private set; }

        public Snippet SelectedSnippet
        {
            get => selectedSnippet_;
            set => SetProperty(ref selectedSnippet_, value);
        }

        public ICommand SettingsClickCommand { get; private set; }

        public TextDocument SnippetCode
        {
            get => snippetCode_;
            set
            {
                SetProperty(ref snippetCode_, value);
                OnPropertyChanged("SnippetCode");
            }
        }

        public DateTime SnippetCreationTime
        {
            get => snippetCreationTime_;
            set
            {
                SetProperty(ref snippetCreationTime_, value);
                OnPropertyChanged("SnippetCreationTime");
            }
        }

        public string SnippetDescription
        {
            get => snippetDescription_;
            set
            {
                SetProperty(ref snippetDescription_, value);
                OnPropertyChanged("SnippetDescription");
            }
        }

        public string SnippetFlag
        {
            get => snippetFlag_;
            set
            {
                SetProperty(ref snippetFlag_, value);
                OnPropertyChanged("SnippetFlag");
            }
        }

        public string SnippetId
        {
            get => snippetId_;
            set
            {
                SetProperty(ref snippetId_, value);
                OnPropertyChanged("SnippetId");
            }
        }

        public string SnippetLanguage
        {
            get => snippetLanguage_;
            set
            {
                SetProperty(ref snippetLanguage_, value);
                OnPropertyChanged("SnippetLanguage");
            }
        }

        public DateTime SnippetLastEditTime
        {
            get => snippetLastEditTime_;
            set
            {
                SetProperty(ref snippetLastEditTime_, value);
                OnPropertyChanged("SnippetLastEditTime");
            }
        }

        public List<Snippet> SnippetList
        {
            get => snippetList_;
            set => SetProperty(ref snippetList_, value);
        }

        public ObservableCollection<string> SnippetTags
        {
            get => snippetTags_;
            set
            {
                SetProperty(ref snippetTags_, value);
                OnPropertyChanged("SnippetTags");
            }
        }

        public string SnippetTitle
        {
            get => snippetTitle_;
            set
            {
                SetProperty(ref snippetTitle_, value);
                OnPropertyChanged("SnippetTitle");
            }
        }

        public ObservableCollection<string> TabControlContent
        {
            get => tabControlContent_;
            set => SetProperty(ref tabControlContent_, value);
        }

        public string TabControlCurrentSelection
        {
            get => tabControlCurrentSelection_;
            set => SetProperty(ref tabControlCurrentSelection_, value);
        }

        public ICommand TabItemSelectedCommand { get; private set; }

        public string WindowTitle
        {
            get => windowTitle_;
            set
            {
                SetProperty(ref windowTitle_, value);
                OnPropertyChanged("WindowTitle");
            }
        }

        public MainViewModel()
        {
            if (Properties.Settings.Default.UpgradeRequired)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpgradeRequired = false;
                Properties.Settings.Default.Save();
            }

            SetApplicationLogic();
            InitializeCommands();
            SnippetCode = new TextDocument();

            switch (Properties.Settings.Default["StartupCategory"])
            {
                case "Flags": IsFlagToggleChecked = true; break;
                case "Tags": IsTagToggleChecked = true; break;
                default: IsLanguageToggleChecked = true; break;
            }
            TabItemSelectedHandler();

            WindowTitle = "CodeBook";
            EditSaveButtonContent = "Edit";
            IsSnippetReadOnly = true;

            Messenger.Default.Register<SettingsClosedRequest>(this, _request =>
            {
                isSettingsWindowOpened_ = false;
            });

            var accent = ThemeManager.Accents.Where(_accent => _accent.Name == Properties.Settings.Default["Accent"].ToString()).SingleOrDefault();
            new AccentColorMenuData()
            {
                Name = accent.Name,
                ColorBrush = accent.Resources["AccentColorBrush"] as Brush
            }.DoChangeTheme();

            var theme = ThemeManager.AppThemes.Where(_theme => _theme.Name == Properties.Settings.Default["Theme"].ToString()).SingleOrDefault();
            new AppThemeMenuData()
            {
                Name = theme.Name,
                BorderColorBrush = theme.Resources["BlackColorBrush"] as Brush,
                ColorBrush = theme.Resources["WhiteColorBrush"] as Brush
            }.DoChangeTheme();
        }

        public async void AddTagHandler()
        {
            if (string.IsNullOrEmpty(AddTagTextBoxContent)) return;

            if (SnippetTags.Contains(AddTagTextBoxContent))
            {
                var metroWindow = Application.Current.MainWindow as MetroWindow;
                var result = await metroWindow.ShowMessageAsync("Duplicate tag",
                    "Cannot add duplicate tag!", MessageDialogStyle.Affirmative);
            }
            else
                SnippetTags.Add(AddTagTextBoxContent);
            AddTagTextBoxContent = string.Empty;
        }

        public void ApplicationClosingHandler()
        {
            var metroWindow = Application.Current.MainWindow as MetroWindow;
            metroWindow.ShowProgressAsync("Cleaning", "Cleaning database, please wait...", false);
            ApplicationLogic.DatabaseLogic.CleanUpDatabase();
        }

        public async void CancelClickHandler()
        {
            if (IsSnippetReadOnly) IsFlyoutOpen = false;
            else
            {
                var metroWindow = Application.Current.MainWindow as MetroWindow;
                var result = await metroWindow.ShowMessageAsync("Unsaved changes",
                    "Do you want to cancel? All changes will be lost.", MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Negative) return;

                if (IsNewSnippet)
                {
                    var index = HamburgerMenuSelectedIndex;
                    HamburgerMenuSelectedIndex = 0;
                    SnippetList.RemoveAt(index);
                    IsNewSnippet = false;
                }

                IsSnippetReadOnly = true;
                LoadSelectedSnippet();
            }
        }

        public void CopySnippetCodeHandler()
        {
            Clipboard.SetText(SnippetCode.Text);
        }

        public async void DeleteSnippetHandler()
        {
            var metroWindow = Application.Current.MainWindow as MetroWindow;
            var result = await metroWindow.ShowMessageAsync("Delete snippet",
                "Do you really want to delete this snippet?", MessageDialogStyle.AffirmativeAndNegative);

            if (result == MessageDialogResult.Affirmative)
            {
                if (!ApplicationLogic.SnippetLogic.DeleteSnippet(SnippetList[HamburgerMenuSelectedIndex].SnippetId))
                {
                    MessageBox.Show("Error occured during snippet deleting. Check application log for details.", "CodeBook",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                SnippetList.RemoveAt(HamburgerMenuSelectedIndex);
                TabItemSelectedHandler();
            }
        }

        public void DeleteTagHandler(string _tag)
        {
            SnippetTags.Remove(_tag);
        }

        public void EditSaveClickHandler()
        {
            if (IsSnippetReadOnly)
            {
                if (SelectedSnippet != null) IsSnippetReadOnly = false;
                else return;
            }
            else
            {
                if (string.IsNullOrEmpty(SnippetLanguage)
                    || string.IsNullOrEmpty(SnippetFlag))
                {
                    var metroWindow = Application.Current.MainWindow as MetroWindow;
                    metroWindow.ShowMessageAsync("Invalid values", "Some of the values are invalid.");
                    return;
                }

                if (SelectedSnippet.Language.LanguageName != SnippetLanguage)
                {
                    SelectedSnippet.Language = ApplicationLogic.LanguageLogic.LanguageExists(SnippetLanguage)
                        ? ApplicationLogic.LanguageLogic.GetLanguage(SnippetLanguage)
                        : new Language(SnippetLanguage);
                }

                if (SelectedSnippet.Flag.FlagName != SnippetFlag)
                {
                    SelectedSnippet.Flag = ApplicationLogic.FlagLogic.FlagExists(SnippetFlag)
                        ? ApplicationLogic.FlagLogic.GetFlag(SnippetFlag)
                        : new Flag(SnippetFlag);
                }

                SelectedSnippet.Code = SnippetCode.Text;
                SelectedSnippet.LastEditTime = DateTime.Now;
                SelectedSnippet.SnippetDescription = SnippetDescription;

                if (IsNewSnippet)
                {
                    if (!ApplicationLogic.SnippetLogic.AddSnippet(SelectedSnippet))
                    {
                        MessageBox.Show("Error occured while saving snippet. Check application log for details.",
                            "CodeBook", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        IsSnippetReadOnly = true;
                        IsNewSnippet = false;
                    }
                }
                else
                {
                    if (!ApplicationLogic.SnippetLogic.ModifySnippet(SelectedSnippet))
                    {
                        MessageBox.Show("Error occured while saving snippet. Check application log for details.",
                            "CodeBook", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else IsSnippetReadOnly = true;
                }

                var oldTags = ApplicationLogic.TagAssignmentLogic.GetSnippetTags(SelectedSnippet.SnippetId).ToList();
                foreach (var oldTag in oldTags)
                {
                    if (!SnippetTags.Contains(oldTag) && !ApplicationLogic.TagAssignmentLogic.UnassignTagFromSnippet(
                            SelectedSnippet.SnippetId,
                            oldTag))
                    {
                        MessageBox.Show(
                            "Error occured while saving tags. Possible data loss. Check application log for details.",
                            "CodeBook", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                }
                foreach (var newTag in SnippetTags)
                {
                    if (!oldTags.Contains(newTag) && !ApplicationLogic.TagAssignmentLogic.AssignTagToSnippet(SelectedSnippet.SnippetId, newTag))
                    {
                        MessageBox.Show(
                            "Error occured while saving tags. Possible data loss. Check application log for details.",
                            "CodeBook", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                }

                string selection = null;
                if (IsFlagToggleChecked)
                    selection = SelectedSnippet.Flag.FlagName;
                else if (IsLanguageToggleChecked)
                    selection = SelectedSnippet.Language.LanguageName;

                ReloadAll();
                IsFlyoutOpen = false;

                if (selection != null) TabControlCurrentSelection = selection;
            }
        }

        public void HamburgerMenuItemClickedHandler()
        {
            LoadSelectedSnippet();
        }

        public async void HamburgerMenuOptionsItemClickedHandler()
        {
            if (!IsNewSnippet)
            {
                var metroWindow = Application.Current.MainWindow as MetroWindow;
                var title = await metroWindow.ShowInputAsync("New snippet", "Enter new snippet title:");
                if (string.IsNullOrEmpty(title)) return;

                SnippetList.Add(new Snippet(new Language("ewfiejgw3g"), new Flag("rfh3rh4thjt4"), title, "Code!", string.Empty));
                HamburgerMenuSelectedIndex = SnippetList.Count - 1;
                LoadSelectedSnippet();
                SnippetLanguage = string.Empty;
                SnippetFlag = string.Empty;
                IsSnippetReadOnly = false;
                IsNewSnippet = true;
                IsFlyoutOpen = true;
            }
        }

        public void OpenFlyoutHandler()
        {
            if (SelectedSnippet != null)
                IsFlyoutOpen = true;
        }

        public async void SearchHandler()
        {
            if (string.IsNullOrEmpty(SearchBoxContent)) return;

            TurnOffToggles();

            var phrase = SearchBoxContent.ToLower();

            SnippetList = ApplicationLogic.SnippetLogic.GetAllSnippets()
                .Where(_snippet =>
                {
                    return _snippet.Title.ToLower().Contains(phrase)
                           || _snippet.Language.LanguageName.ToLower().Contains(phrase)
                           || _snippet.Flag.FlagName.ToLower().Contains(phrase)
                           || _snippet.Code.ToLower().Contains(phrase)
                           || _snippet.SnippetDescription.ToLower().Contains(phrase)
                           || _snippet.CreationTime.ToString().ToLower().Contains(phrase)
                           || _snippet.LastEditTime.ToString().ToLower().Contains(phrase);
                }).ToList();

            TabControlContent = new ObservableCollection<string>(new List<string>() { "Search results: " + SearchBoxContent });
            TabControlCurrentSelection = TabControlContent[0];

            if (SnippetList.Count() < 1)
            {
                var metroWindow = Application.Current.MainWindow as MetroWindow;
                var result = await metroWindow.ShowMessageAsync("Search Results not matched", "There are no results for :  " + SearchBoxContent);
            }
        }

        public void SettingsClickHandler()
        {
            if (isSettingsWindowOpened_) return;
            new SettingsWindow().Show();
            isSettingsWindowOpened_ = true;
        }

        public void TabItemSelectedHandler(SelectionChangedEventArgs _args = null)
        {
            if (_args != null)
            {
                if (!(_args.OriginalSource is MetroAnimatedSingleRowTabControl)) return;
                _args.Handled = true;
            }

            if (IsFlagToggleChecked)
            {
                SnippetList = ApplicationLogic.SnippetLogic.GetAllSnippetsWithFlag(TabControlCurrentSelection)?.ToList();
            }
            else if (IsLanguageToggleChecked)
            {
                SnippetList = ApplicationLogic.SnippetLogic.GetAllSnippetsWithLanguage(TabControlCurrentSelection)?.ToList();
            }
            else if (IsTagToggleChecked)
            {
                SnippetList = ApplicationLogic.SnippetLogic.GetAllSnippetsWithTag(TabControlCurrentSelection)?.ToList();
            }

            if (SnippetList == null) SnippetList = new List<Snippet>();
            HamburgerMenuSelectedIndex = 0;
            LoadSelectedSnippet();
        }
    }
}