﻿using System;
using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Logic
{
    public interface ISnippetLogic
    {
        bool AddSnippet(Snippet _snippet);

        bool DeleteAllSnippets();

        bool DeleteSnippet(Guid _snippetId);

        IEnumerable<Snippet> GetAllMatchingSnippets(Func<Snippet, bool> _predicate);

        IEnumerable<Snippet> GetAllSnippets();

        IEnumerable<Snippet> GetAllSnippetsWithFlag(string _flagName);

        IEnumerable<Snippet> GetAllSnippetsWithLanguage(string _languageName);

        IEnumerable<Snippet> GetAllSnippetsWithTag(string _tagName);

        IEnumerable<Snippet> GetAllSnippetsWithTitle(string _title);

        bool ModifySnippet(Snippet _snippet);

        bool SnippetExists(Guid _snippetId);
    }
}