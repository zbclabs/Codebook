﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Repository;
using log4net;
using Logic.Core;
using Moq;
using NUnit.Framework;

namespace Logic.Tests.Core
{
    [TestFixture]
    public class FlagLogicTests
    {
        private FlagLogic flagLogic_;
        private Mock<IFlagRepository> flagRepositoryMock_;
        private Mock<ILog> loggerMock_;

        [Test, Category("Unit")]
        public void AddFlag_CorrectFlagName()
        {
            var flagName = "Done";
            var flag = new Flag(flagName);
            flagRepositoryMock_.Setup(_x => _x.AddFlag(flagName)).Returns(true);
            flagRepositoryMock_.Setup(_x => _x.GetFlag(flagName)).Returns(flag);
            Assert.AreEqual(flag, flagLogic_.AddFlag(flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            flagRepositoryMock_.Verify(_x => _x.AddFlag(flagName), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void AddFlag_IncorrectFlagName(string _flagName)
        {
            Assert.AreEqual(null, flagLogic_.AddFlag(_flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.AddFlag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteAllFlags()
        {
            flagRepositoryMock_.Setup(_x => _x.DeleteAllFlags()).Returns(true);
            Assert.AreEqual(true, flagLogic_.DeleteAllFlags());
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            flagRepositoryMock_.Verify(_x => _x.DeleteAllFlags(), Times.Once);
        }

        [Test, Category("Unit")]
        public void DeleteFlag_FlagDoesntExist()
        {
            var flag = "Done";
            flagRepositoryMock_.Setup(_x => _x.FlagExists(flag)).Returns(false);
            Assert.AreEqual(false, flagLogic_.DeleteFlag(flag));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.DeleteFlag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void DeleteFlag_FlagExists()
        {
            var flag = "Done";
            flagRepositoryMock_.Setup(_x => _x.FlagExists(flag)).Returns(true);
            flagRepositoryMock_.Setup(_x => _x.DeleteFlag(flag)).Returns(true);
            Assert.AreEqual(true, flagLogic_.DeleteFlag(flag));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Never);
            flagRepositoryMock_.Verify(_x => _x.DeleteFlag(It.IsAny<string>()), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void DeleteFlag_IncorrectFlagName(string _flagName)
        {
            Assert.AreEqual(false, flagLogic_.DeleteFlag(_flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(x => x.DeleteFlag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void FlagExists_CorrectFlagName()
        {
            var flag = "Done";
            flagLogic_.FlagExists(flag);
            flagRepositoryMock_.Verify(_x => _x.FlagExists(flag), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void FlagExists_IncorrectFlagName(string _flagName)
        {
            Assert.AreEqual(false, flagLogic_.FlagExists(_flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.FlagExists(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        public void FlagLogicConstructor_IncorrectArguments(bool _isLoggerNull, bool _isFlagRepositoryNull)
        {
            var logger = _isLoggerNull ? null : loggerMock_.Object;
            var flagRepository = _isFlagRepositoryNull ? null : flagRepositoryMock_.Object;

            try
            {
                var flagLogic = new FlagLogic(logger, flagRepository);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
        }

        [Test, Category("Unit")]
        public void GetAllFlags()
        {
            var flags = new List<string> { "ToDo", "InProgress", "Done" };
            flagRepositoryMock_.Setup(_x => _x.GetAllFlags()).Returns(flags);
            Assert.AreEqual(flags, flagLogic_.GetAllFlags());
        }

        [Test, Category("Unit")]
        public void GetFlag_CorrectFlagName()
        {
            var flag = "Done";
            flagLogic_.GetFlag(flag);
            flagRepositoryMock_.Verify(_x => _x.GetFlag(flag), Times.Once);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void GetFlag_IncorrectFlagName(string _flagName)
        {
            Assert.AreEqual(null, flagLogic_.GetFlag(_flagName));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.GetFlag(It.IsAny<string>()), Times.Never);
        }

        [Test, Category("Unit")]
        public void ModifyFlag_CorrectFlag()
        {
            var flag = new Flag("Done");
            flagLogic_.ModifyFlag(flag);
            flagRepositoryMock_.Verify(_x => _x.ModifyFlag(flag), Times.Once);
        }

        [Test, Category("Unit")]
        public void ModifyFlag_NullFlag()
        {
            Assert.AreEqual(false, flagLogic_.ModifyFlag(null));
            loggerMock_.Verify(_x => _x.Warn(It.IsAny<string>()), Times.Once);
            flagRepositoryMock_.Verify(_x => _x.ModifyFlag(It.IsAny<Flag>()), Times.Never);
        }

        [SetUp]
        public void SetUp()
        {
            flagRepositoryMock_ = new Mock<IFlagRepository>();
            loggerMock_ = new Mock<ILog>();
            loggerMock_.Setup(_x => _x.Warn(It.IsAny<string>()));
            flagLogic_ = new FlagLogic(loggerMock_.Object, flagRepositoryMock_.Object);
        }
    }
}