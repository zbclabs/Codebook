﻿using Api.Interfaces.Logic;
using log4net;
using Logic.Core;
using Ninject.Modules;

namespace Logic.Ninject
{
    public class LogicModule : NinjectModule
    {
        private readonly ILog logger_;

        public LogicModule(ILog _logger)
        {
            logger_ = _logger;
        }

        public override void Load()
        {
            Bind<IApplicationLogic>()
                .To<ApplicationLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<IDatabaseLogic>()
                .To<DatabaseLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<IFlagLogic>()
                .To<FlagLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<ILanguageLogic>()
                .To<LanguageLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<ISnippetLogic>()
                .To<SnippetLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<ITagAssignmentLogic>()
                .To<TagAssignmentLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);

            Bind<ITagLogic>()
                .To<TagLogic>()
                .InSingletonScope()
                .WithConstructorArgument("_logger", logger_);
        }
    }
}