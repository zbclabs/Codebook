﻿using System.Collections.Generic;
using Api.Dto;

namespace Api.Interfaces.Logic
{
    public interface ILanguageLogic
    {
        Language AddLanguage(string _languageName);

        bool DeleteAllLanguages();

        bool DeleteLanguage(string _languageName);

        IEnumerable<string> GetAllLanguages();

        Language GetLanguage(string _languageName);

        bool LanguageExists(string _languageName);

        bool ModifyLanguage(Language _language);
    }
}