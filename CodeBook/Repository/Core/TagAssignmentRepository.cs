﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Api.Interfaces.Repository;
using Dapper;
using log4net;

namespace Repository.Core
{
    public class TagAssignmentRepository : BaseRepository, ITagAssignmentRepository
    {
        public TagAssignmentRepository(string _connectionString, ILog _logger) : base(_connectionString, _logger)
        {
        }

        public bool AssignTagToSnippet(Guid _snippetId, int _tagId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        TagAssignmentId = Guid.NewGuid().ToString(),
                        SnippetId = _snippetId.ToString(),
                        TagId = _tagId
                    };
                    connection.Execute(SqlDictionary.AssignTagToSnippet, @params);
                    return true;
                }
            }, false);
        }

        public bool DeleteAllAssignments()
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    connection.Execute(SqlDictionary.DeleteAllAssignments);
                    return true;
                }
            }, false);
        }

        public IEnumerable<string> GetSnippetTags(Guid _snippetId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString()
                    };
                    return connection.Query<string>(SqlDictionary.GetSnippetTags, @params);
                }
            }, null);
        }

        public bool TagAssignmentExists(Guid _snippetId, int _tagId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString(),
                        TagId = _tagId
                    };
                    return connection.Query<bool>(SqlDictionary.TagAssignmentExists, @params).Single();
                }
            }, false);
        }

        public bool UnassignTagFromSnippet(Guid _snippetId, int _tagId)
        {
            return ExecuteInLoggerTry(() =>
            {
                using (var connection = new SQLiteConnection(ConnectionString))
                {
                    var @params = new
                    {
                        SnippetId = _snippetId.ToString(),
                        TagId = _tagId
                    };
                    connection.Execute(SqlDictionary.UnassignTagFromSnippet, @params);
                    return true;
                }
            }, false);
        }
    }
}