﻿using log4net;
using Ninject;

namespace Logic.Ninject
{
    public class CodeBookKernel : StandardKernel
    {
        public CodeBookKernel(string _databaseFilePath, ILog _logger)
            : base(new RepositoryModule(_databaseFilePath, _logger), new LogicModule(_logger))
        {
        }
    }
}