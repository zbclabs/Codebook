﻿using System;
using System.Globalization;
using System.Windows.Data;
using ICSharpCode.AvalonEdit.Highlighting;

namespace UserExperience.Converters
{
    public class SyntaxHighlightingConverter : IValueConverter
    {
        public object Convert(object _value, Type _targetType, object _parameter, CultureInfo _culture)
        {
            if (_targetType != typeof(IHighlightingDefinition))
                throw new InvalidOperationException("Unsupported type.");

            return _value != null ? new HighlightingDefinitionTypeConverter().ConvertFrom(_value) : null;
        }

        public object ConvertBack(object _value, Type _targetType, object _parameter, CultureInfo _culture)
        {
            if (!(_targetType != typeof(string)))
                throw new InvalidOperationException("Unsupported type.");

            return ((IHighlightingDefinition)_value)?.Name;
        }
    }
}