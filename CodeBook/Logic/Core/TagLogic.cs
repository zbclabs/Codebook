﻿using System;
using System.Collections.Generic;
using Api.Dto;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class TagLogic : ITagLogic
    {
        private readonly ILog logger_;
        private readonly ITagRepository tagRepository_;

        public TagLogic(ILog _logger, ITagRepository _tagRepository)
        {
            logger_ = _logger ?? throw new ArgumentException("Logger cannot be null!");
            tagRepository_ = _tagRepository ?? throw new ArgumentException("TagRepository cannot be null!");
        }

        public Tag AddTag(string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning null...");
                return null;
            }
            tagRepository_.AddTag(_tagName);
            return tagRepository_.GetTag(_tagName);
        }

        public bool DeleteAllTags()
        {
            return tagRepository_.DeleteAllTags();
        }

        public bool DeleteTag(string _tagName)
        {
            if (string.IsNullOrEmpty(_tagName))
            {
                logger_.Warn("TagName is null or empty, returning false...");
                return false;
            }
            if (tagRepository_.TagExists(_tagName))
                return tagRepository_.DeleteTag(_tagName);
            logger_.Warn("Specified tag doesn't exist, no tag has been deleted.");
            return false;
        }

        public IEnumerable<string> GetAllTags()
        {
            return tagRepository_.GetAllTags();
        }

        public Tag GetTag(string _tagName)
        {
            if (!string.IsNullOrEmpty(_tagName)) return tagRepository_.GetTag(_tagName);
            logger_.Warn("TagName is null or empty, returning null...");
            return null;
        }

        public bool ModifyTag(Tag _tag)
        {
            if (_tag != null) return tagRepository_.ModifyTag(_tag);
            logger_.Warn("Tag is null, returning false...");
            return false;
        }

        public bool TagExists(string _tagName)
        {
            if (!string.IsNullOrEmpty(_tagName)) return tagRepository_.TagExists(_tagName);
            logger_.Warn("TagName is null or empty, returning false...");
            return false;
        }
    }
}