﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Dto;
using Api.Interfaces.Logic;
using Api.Interfaces.Repository;
using log4net;

namespace Logic.Core
{
    public class DatabaseLogic : IDatabaseLogic
    {
        private readonly IDatabaseRepository databaseRepository_;
        private readonly IFlagRepository flagRepository_;
        private readonly ILanguageRepository languageRepository_;
        private readonly ISnippetRepository snippetRepository_;
        private readonly ITagRepository tagRepository_;

        public DatabaseLogic(
            IDatabaseRepository _databaseRepository,
            IFlagRepository _flagRepository,
            ILanguageRepository _languageRepository,
            ISnippetRepository _snippetRepository,
            ITagRepository _tagRepository)
        {
            databaseRepository_ = _databaseRepository ?? throw new ArgumentException("DatabaseRepository cannot be null!");
            flagRepository_ = _flagRepository ?? throw new ArgumentException("FlagRepository cannot be null!");
            languageRepository_ = _languageRepository ?? throw new ArgumentException("LanguageRepository cannot be null!");
            snippetRepository_ = _snippetRepository ?? throw new ArgumentException("SnippetRepository cannot be null!");
            tagRepository_ = _tagRepository ?? throw new ArgumentException("TagRepository cannot be null!");
        }

        public bool CleanUpDatabase()
        {
            var flags = flagRepository_.GetAllFlags()?.ToList();
            if (flags == null) return false;
            List<Snippet> snippets;

            Flag flag;
            foreach (var flagName in flags)
            {
                flag = flagRepository_.GetFlag(flagName);
                if (flag == null) return false;
                snippets = snippetRepository_.GetAllSnippetsWithFlag(flag.FlagId)?.ToList();
                if (snippets == null) return false;
                if (snippets.Count == 0)
                    flagRepository_.DeleteFlag(flagName);
            }

            var languages = languageRepository_.GetAllLanguages()?.ToList();
            if (languages == null) return false;

            Language language;
            foreach (var languageName in languages)
            {
                language = languageRepository_.GetLanguage(languageName);
                if (language == null) return false;
                snippets = snippetRepository_.GetAllSnippetsWithLanguage(language.LanguageId)?.ToList();
                if (snippets == null) return false;
                if (snippets.Count == 0)
                    languageRepository_.DeleteLanguage(languageName);
            }

            var tags = tagRepository_.GetAllTags()?.ToList();
            if (tags == null) return false;

            Tag tag;
            foreach (var tagName in tags)
            {
                tag = tagRepository_.GetTag(tagName);
                if (tag == null) return false;
                snippets = snippetRepository_.GetAllSnippetsWithTag(tag.TagId)?.ToList();
                if (snippets == null) return false;
                if (snippets.Count == 0)
                    tagRepository_.DeleteTag(tagName);
            }

            return true;
        }

        public bool CreateDatabase()
        {
            return databaseRepository_.CreateDatabase();
        }

        public bool DropDatabase()
        {
            return databaseRepository_.DropDatabase();
        }
    }
}